
import React, { useEffect, useState, useRef } from 'react';

import { Menu, Row, Col, Button, Input, Spin, Radio, Modal, Form, Select, Table, Space, Card, Tabs, Divider, Switch } from 'antd';
import { get, isEmpty } from 'lodash';
import {
    DownOutlined, UpOutlined, PlusCircleOutlined, MenuOutlined, DeleteOutlined
} from '@ant-design/icons';
import MathJax from 'react-mathjax-preview';
import arrayMove from 'array-move';
import { SortableContainer, SortableElement } from 'react-sortable-hoc';


import request from '../../handler/request';

const LessonPart = (props) => {
    const { activeKey, setActive, data } = props;

    const [state, setState] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        if (activeKey == '0') {
            requestLesson()
        }
    }, [activeKey])

    useEffect(() => {
        if (get(data, 'id', '')) {
            requestLesson();
        }

    }, [data]);


    const requestLesson = () => {
        if (get(data, 'id', '')) {
            setLoading(true);
            request.get(`/admin/lessons/${get(data, 'id', '')}`)
                .then(({ data: lessons }) => {
                    console.log(get(lessons, 'parts', []), '-==')
                    setState(get(lessons, 'parts', []));
                    setTimeout(() => setLoading(false), 250)
                })
        }
    }

    const saveOrder = () => {
        const dataPost = state.map(i => i.id);
        if (isEmpty(dataPost)) return true;
        setLoading(true)
        request.post(`/admin/lessons/${get(data, 'id', '')}/sort-parts`, {}, { parts: dataPost })
            .then(({ response }) => {
                setState(get(response, 'data.data.parts', []));
                setTimeout(() => setLoading(false), 250)
            })
    }

    const onSortEnd = ({ oldIndex, newIndex }) => {
        setState(arrayMove(state, oldIndex, newIndex));
    };

    return (
        <div>
            <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                <Button type="primary"
                    onClick={saveOrder}
                    icon={< PlusCircleOutlined />}
                > Lưu thứ tự</Button>
                <Button onClick={requestLesson} loading={loading} >Reload</Button>
            </div>
            {
                isEmpty(state) ? null :
                    <SortableList items={state} onClick={setActive} data={data} onSortEnd={onSortEnd} />
            }
        </div >
    )
};
const mapTab = {
    'article': '1',
    'exam': '3',
    'lecture': '2'
}
const SortableItem = SortableElement(({ data, onClick, type = '' }) => {
    return (
        <div style={{ margin: '15px 0px' }}>
            <Card
                size="small"
                title={null}
            >
                <div>
                    <MenuOutlined style={{ padding: '0 10px' }} />
                    <span style={{
                        fontSize: '20px',
                        textTransform: 'capitalize'
                    }}>{type}: {get(data, 'title', '')}</span>
                </div>
            </Card>
        </div>
    )
    if (type === 'article') {
        return (
            <RenderArticle data={data} onClick={() => onClick('1')} />
        )

    } else if (type === 'exam') {
        return (
            <RenderExam data={data} onClick={() => onClick('3')} />
        )
    } else {
        return <RenderExam data={data} onClick={() => onClick('2')} />
    }
});

const SortableList = SortableContainer(({ items, onClick }) => {
    return (
        <div>
            {
                items.map((item, index) => {
                    const data = get(item, 'partable', {});
                    return <SortableItem key={String(item.id)} data={data} onClick={onClick} index={index} type={get(item, 'type', '')} />
                })
            }
        </div>
    );
});

const RenderArticle = ({ data, onClick }) => {
    const [expand, setExpand] = useState(false);

    return (
        <div style={{ margin: '15px 0px' }}>
            <Card title={
                <div>
                    <span>article: </span>
                    <span>{get(data, 'title', '')}</span>
                </div>
            }
                extra={
                    <div>
                        <Button
                            onClick={onClick}
                        >
                            Xem tất cả
                        </Button>
                        <Button
                            icon={expand ? <DownOutlined /> : <UpOutlined />}
                            onClick={() => setExpand(!expand)}
                            style={{ marginLeft: 20 }}
                        />

                    </div>
                }
            >
                {
                    expand ?
                        <div>
                            <MathJax math={get(data, 'content', '')} />
                        </div>
                        : null
                }
            </Card>
        </div>
    )
}

const RenderExam = ({ data, onClick }) => {
    const [expand, setExpand] = useState(false);
    return (
        <div style={{ margin: '15px 0px' }}>
            <Card title={
                <div>
                    <span>Exam: </span>
                    <span>{get(data, 'title', '')}</span>
                </div>
            }
                extra={
                    <div>
                        <Button
                            onClick={onClick}
                        >
                            Xem tất cả
                        </Button>
                        <Button
                            icon={expand ? <DownOutlined /> : <UpOutlined />}
                            onClick={() => setExpand(!expand)}
                            style={{ marginLeft: 20 }}
                        />

                    </div>
                }
            >
                {
                    expand ?
                        <div>
                            <MathJax math={get(data, 'content', '')} />
                        </div>
                        : null
                }
            </Card>
        </div>
    )

}

export default LessonPart;