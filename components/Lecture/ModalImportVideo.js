import { useState, useCallback, useEffect, useRef } from 'react';

import { Menu, Row, Col, Button, Input, Spin, Switch, Modal, Form, Select, Table, Space, Card, notification } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { useSpring, animated } from 'react-spring'
import {
    PlayCircleFilled,
    PauseCircleOutlined,
    DeleteFilled
} from '@ant-design/icons';
import styled from 'styled-components';
import { useRouter } from 'next/router'
import { get, constant, rest } from 'lodash';
import ReactPlayer from 'react-player';
const { TextArea } = Input;



// Render a YouTube video player

import request from '../../handler/request';
import SearchTable from '../share/SearchTable';
// import '../public/styles.css';

const ModalImport = ({ visible, handleCancel, handleOk, lesson_id }) => {
    console.log('visiblevisiblevisiblevisible', visible)
    const formImport = Form.useForm()[0];
    const formRef = useRef();
    const [loading, setLoading] = useState(false);
    const openNotificationWithIcon = (type, message = '') => notification[type]({ message });

    const onFinishFrom = (values) => {

        if (isNaN(values.curriculum_id)) { //valid duration
            formRef.current.setFields([
                {
                    name: 'id',
                    errors: ['Id video phải là số'],
                    touched: true
                }
            ])
            return 0;
        }
        setLoading(true);

        request.post('/admin/videos/import', {}, {
            ...values,
            lecture_id: get(visible, 'id', ''),
        })
            .then((data) => {
                openNotificationWithIcon('success', 'Tạo video thành công');
                setTimeout(() => { setLoading(false); formImport.resetFields() }, 200);
                handleOk()
            })
            .catch(err => {
                console.log(err)
                openNotificationWithIcon('error', 'Tạo video thất bại');
                setTimeout(() => { setLoading(false) }, 200)
            })

        // handle ==== call api
    }

    return (
        <Modal
            title={'Import Video'}
            visible={visible}
            width='60%'
            onCancel={handleCancel}
            footer={null}
        >
            <Card
            // title={typeForm == 'add' ? "Tạo lớp mới mới" : "Chỉnh sửa thông tin lớp"}
            // extra={<a onClick={() => setTypeForm('add')} href="#">Add new</a>}
            >
                <Form
                    ref={formRef}
                    form={formImport}
                    name="control-hooks"
                    onFinish={onFinishFrom}
                    labelCol={{ span: 6 }}
                // layout="vertical"
                >
                    {
                        formJson.map((item, index) => {
                            return (
                                <Form.Item key={String(index)} name={item.name} label={item.label} rules={item.rules} style={item.hiden ? { display: 'none' } : {}} >
                                    <Input placeholder={item.placeHolder}/>
                                </Form.Item>
                            )
                        })
                    }

                    <Form.Item
                        wrapperCol={{ offset: 6 }}
                    >
                        <Button
                            type="primary"
                            htmlType="submit"
                            style={{ marginRight: '20px' }}
                            loading={loading}
                        >
                            Thêm video
                        </Button>
                        <Button
                            style={{ marginRight: '20px' }}
                            loading={loading}
                            onClick={() => { formImport.resetFields() }}
                        >
                            Reset
                        </Button>
                    </Form.Item>

                </Form>
            </Card>
        </Modal>

    )
}

export default ModalImport;


const formJson = [
    {
        name: 'title',
        label: 'Title',
        rules: [{ required: true, min: 3 }],
    },
    {
        name: 'curriculum_id',
        label: 'Id video',
        rules: [{ required: true }],
        placeHolder: 'ID bài học trên trang khoahoc.vietjack.com'
    },
]
