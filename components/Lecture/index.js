import { useState, useCallback, useEffect, useRef } from 'react';

import { Menu, Row, Col, Button, Input, Spin, Switch, Modal, Form, Select, Table, Space, Card, notification } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { useSpring, animated } from 'react-spring'
import {
    PlayCircleFilled,
    PauseCircleOutlined,
    DeleteFilled,
    EditOutlined,
    PlusCircleFilled
} from '@ant-design/icons';
import styled from 'styled-components';
import { useRouter } from 'next/router'
import { get, constant, rest } from 'lodash';
import ModalVideo from './ModalVideo';
import ModalImport from './ModalImportVideo';

// Render a YouTube video player

import request from '../../handler/request';
import SearchTable from '../share/SearchTable';
import {
    gradesFrom,
    columnsGrades
} from './const';
// import '../public/styles.css';

const { Option } = Select;
const { TextArea } = Input;

// ================================================================================
const App = ({ data }) => {
    const router = useRouter();
    const formClassRef = useRef();
    const [dataForm] = Form.useForm();
    const [isLoading, setLoading] = useState(false);
    const [dataClass, setDataClass] = useState([]);
    const [active, setActive] = useState({});

    const userInfo = useSelector(state => state.userInfo);
    const [isAdmin, setIsAdmin] = useState(get(userInfo, 'user.role', '') === 'admin');
    const [columnsData, setColumn] = useState(columnsGrades);
    const [visible, setVisible] = useState(false);
    const [typeForm, setTypeForm] = useState('add');

    const [jsonForm, setJsonForm] = useState(gradesFrom);
    const [showImport, setShowImport] = useState(false);

    useEffect(() => {
        if (get(userInfo, 'user.role', ''))
            setIsAdmin(get(userInfo, 'user.role_name', '') === 'admin')
        else
            setIsAdmin(localStorage.getItem('role') === 'admin')
    }, [userInfo]);

    const openNotificationWithIcon = (type, message = '') => {
        notification[type]({ message });
    };

    const _handleClickEdit = useCallback((data) => {
        setTypeForm('edit');
        toggle(!scale);
        // formDataRef.current.setFieldsValue(data);
        dataForm.setFieldsValue(data);
        setActive({
            is_actived: Boolean(data.is_actived)
        });
    }, [])

    const onFinishFromClass = (values) => {
        const { id = '' } = data;
        setLoading(true)
        if (typeForm == 'add') {
            request.post(`/admin/lectures`, {}, { ...values, lesson_id: id })
                .then(({ response = {} } = {}) => {
                    openNotificationWithIcon('success', 'Tạo lớp thành công');
                    loadData();
                    onResetFormClass();
                })
                .catch(error => {
                    openNotificationWithIcon('error', 'Tạo lớp thất bại');
                    console.log('error creact grades', error);
                    if (error.response && error.response.data) {
                        const listErr = get(error, 'response.data.errors', {});
                        Object.keys(listErr).map((key) => {
                            formClassRef.current.setFields([
                                {
                                    name: key,
                                    errors: listErr[key],
                                },
                            ]);
                        })
                    }
                })
        } else {
            request.post(`/admin/lectures/${values.id}`, {}, { ...values, lesson_id: id })
                .then((dataCreate) => {
                    openNotificationWithIcon('success', 'Chỉnh sửa lớp thành công');
                    loadData();
                    onResetFormClass();
                    setTypeForm('add');
                })
                .catch(error => {
                    openNotificationWithIcon('error', 'Chỉnh sửa lớp thất bại');
                    console.log('error creact grades', error);
                    if (error.response && error.response.data && error.response.data && error.response.data.errors) {
                        const listErr = get(error, 'response.data.errors', {});
                        console.log('error.response.data', error.response.data.errors)
                        Object.keys(listErr).map((key) => {
                            formClassRef.current.setFields([
                                {
                                    name: key,
                                    errors: listErr[key],
                                },
                            ]);
                        })
                    }
                })
        }
        setTimeout(() => setLoading(false), 400)

    }

    const _handleResetForm = () => {
        if (typeForm === 'edit') {
            formClassRef.current.setFieldsValue(dataEdit);
        } else {
            onResetFormClass();
        }
    }

    const onResetFormClass = () => {
        dataForm.resetFields();
    }

    useEffect(() => {
        loadData();
        const columnsConvert = [
            ...columnsGrades,
            {
                title: 'Video',
                key: 'operation',
                fixed: 'right',
                width: 100,
                render: (data) => {
                    return (
                        <div style={{ display: 'flex', justifyContent: 'space-around' }}>
                            {data.videos && <a onClick={() => { setVisible(data.videos) }}><EditOutlined /> </a>}
                            <a onClick={() => { setShowImport(data) }}> <PlusCircleFilled /> </a>
                        </div>
                    )
                }
            },
            {
                title: 'Action',
                key: 'operation',
                fixed: 'right',
                width: 100,
                render: (data) => {
                    return (
                        <div style={{ display: 'flex', justifyContent: 'space-around' }}>
                            <a onClick={() => { _handleClickEdit(data); }}> <EditOutlined /> </a>
                        </div>
                    )
                }
            }
        ];
        setColumn(columnsConvert);

        request.get('/admin/masterdata?teachers=true')
            .then(({ teachers }) => {
                const newJson = jsonForm.map(i => {
                    if (i.name === 'teacher_id') {
                        i.data = teachers;
                    }
                    return i;
                })
                setJsonForm(newJson)
            })
        // /admin/masterdata?subjects=true
    }, [data]);


    const loadData = () => {
        const { id = '' } = data;
        if (!id) return 1;
        setLoading(true)
        request.get(`/admin/lectures?lesson_id=${id}`)
            .then(({ data }) => {
                setDataClass(data.map(i => ({ ...i, key: i.id })));
                setTimeout(() => setLoading(false), 400)
            })
            .catch(error => {
                console.log('error fetch data grades', error)
                setTimeout(() => setLoading(false), 400)
            })
        // Call
    }
    // animation 
    const [scale, toggle] = useState(false)
    const { x } = useSpring({
        from: { x: 0 },
        x: scale ? 1 : 0,
        config: { duration: 1000 }
    })


    return (
        <Row className="site-layout-background" style={{ minHeight: 360, padding: '24px 10px' }}>
            {/* table */}
            <Col span={isAdmin ? 17 : 24} style={{ padding: '0 10px' }}>
                <Card title="Thông tin lớp học" extra={<Button onClick={() => loadData()} loading={isLoading}> Làm mới </Button>} >
                    <SearchTable columns={columnsData} dataSource={dataClass} />
                </Card>
            </Col>
            {/* form */}
            {isAdmin && <Col span={7} style={{ padding: '0 10px' }}>
                <animated.div
                    style={{
                        transform: x
                            .interpolate({
                                range: [0, 0.25, 0.35, 0.45, 0.55, 0.65, 0.75, 1],
                                output: [1, 0.99, 0.98, 1.05, 0.98, 1.05, 1.03, 1]
                            })
                            .interpolate(x => `scale(${x})`)
                    }}>
                    <Card
                        title={typeForm == 'add' ? "Tạo bài học mới" : "Chỉnh sửa bài học"}
                        extra={<a onClick={() => { setTypeForm('add'); onResetFormClass() }} href="#"> Thêm mới</a>}
                    >
                        <Form
                            ref={formClassRef}
                            form={dataForm}
                            name="control-hooks"
                            onFinish={onFinishFromClass}
                            layout="vertical"
                        >
                            {
                                jsonForm.map((item, index) => {
                                    if (item.type === 'select') {
                                        return (
                                            <Form.Item key={String(index)} name={item.name} label={item.label} rules={item.rules} style={item.hidden ? { display: 'none' } : {}} >
                                                <Select
                                                    showSearch
                                                    // style={{ width: 200 }}
                                                    placeholder="Chọn giáo viên"
                                                    optionFilterProp="children"
                                                    filterOption={(input, option) =>
                                                        option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                                    }
                                                >
                                                    {
                                                        get(item, 'data', [])
                                                            .map((icon) => <Option key={icon.id} value={icon.id}>{icon.name}</Option>)
                                                    }
                                                </Select>
                                            </Form.Item>
                                        )
                                    } else if (item.type === 'switch') {
                                        return (
                                            <Form.Item
                                                key={String(index)}
                                                valuePropName={item.name}
                                                name={item.name}
                                                label={item.label}
                                                rules={item.rules}
                                                style={item.hiden ? { display: 'none' } : {}}
                                            >
                                                <Switch checked={active[item.name]} onChange={() => {
                                                    setActive({
                                                        [item.name]: !active[item.name]
                                                    })
                                                }} />
                                            </Form.Item>
                                        )
                                    } else {
                                        return (
                                            <Form.Item key={String(index)} name={item.name} label={item.label} rules={item.rules}
                                                style={item.hidden ? { display: 'none' } : {}}
                                            >
                                                <Input />
                                            </Form.Item>
                                        )
                                    }
                                })
                            }
                            <Form.Item >
                                <Button type="primary" htmlType="submit" style={{ marginRight: '20px' }} loading={isLoading}>
                                    {typeForm == 'add' ? 'Tạo mới' : 'Chỉnh sửa'}
                                </Button>
                                <Button onClick={_handleResetForm} style={{ marginRight: '20px' }}>
                                    Reset
                                </Button>
                            </Form.Item>
                        </Form>
                    </Card>
                </animated.div>
            </Col>}

            <ModalVideo
                visible={visible}
                onCancel={() => { setVisible(false); loadData() }}
                onOk={() => { setVisible(false); loadData() }}
            />
            <ModalImport
                visible={showImport}
                handleOk={() => { setShowImport(false); loadData() }}
                handleCancel={() => setShowImport(false)}
                lesson_id={get(data, 'id', '')}
            />

        </Row>
    )
};
// ================================================================================

export default App;




const Line = () => (
    <div style={{
        height: '22px',
        width: '1px',
        background: '#bfbfbf',
        margin: '0 5px'
    }} />
)
