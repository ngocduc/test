import { useState, useCallback, useEffect, useRef } from 'react';

import { Menu, Row, Col, Button, Input, Spin, Switch, Modal, Form, Select, Table, Space, Card, notification, Divider } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { useSpring, animated } from 'react-spring'
import {
    PlayCircleFilled,
    PauseCircleOutlined,
    DeleteFilled,
    ExclamationCircleOutlined
} from '@ant-design/icons';
import styled from 'styled-components';
import { useRouter } from 'next/router'
import { get, isEmpty } from 'lodash';
import ReactPlayer from 'react-player';
const { TextArea } = Input;



// Render a YouTube video player

import request from '../../handler/request';
import SearchTable from '../share/SearchTable';
// import '../public/styles.css';
const { confirm } = Modal;


const ModalVideo = React.memo(({
    visible,
    onCancel,
    onOk = () => { }
}) => {
    const [playing, setPlay] = useState(false);
    const refVideo = useRef();
    const [isPlay, setPlayItem] = useState('');
    const [listInfo, setListInfo] = useState([]);
    const [urlVideo, setUrl] = useState(get(visible, 'url', ''));

    const [isOkToClose, setIsOk] = useState(true);
    const [formData] = Form.useForm();

    const openNotificationWithIcon = (type, message = '') => {
        notification[type]({ message });
    };

    useEffect(() => {
        const timestamps = get(visible, 'timestamps', [])
        const convertData = timestamps.reduce((car, cur) => {
            car[cur.time] = cur.text;
            return car;
        }, {});
        setListInfo(convertData);

        const { url = '', title = '' } = visible
        if (url) {
            setUrl(url);
            formData.setFieldsValue({
                url,
                title
            })
        }
    }, [visible]);

    const _handleReset = () => {
        const { url = '', title = '', timestamps = [] } = visible;

        const convertData = timestamps.reduce((car, cur) => {
            car[cur.time] = cur.text;
            return car;
        }, {});
        setListInfo(convertData);
        formData.setFieldsValue({ url, title });
    }

    const _handleSave = () => {
        setIsOk(true);
        // 
        const classElm = document.getElementsByClassName('item-info-video');
        for (let i = 0; i < classElm.length; i++) classElm[i].style.border = '1px solid #dedede';

        const indexEmpty = Object.keys(listInfo).find(key => listInfo[key] === '');
        if (indexEmpty !== undefined) {
            openNotificationWithIcon('error', 'Vui lòng điền đầy đủ thông tin đoạn trích');
            const elmntId = document.getElementsByClassName(indexEmpty + 'form');
            elmntId[0].style.border = '1px solid red';
        } else {
            const {
                id,
                length,
            } = visible;
            const { title, url } = formData.getFieldsValue();
            const lenghtVideo = refVideo.current.getDuration();
            console.log(lenghtVideo, length)
            const timestamps = Object.keys(listInfo)
                .map(key => ({
                    text: listInfo[key],
                    time: key,
                }));
            request.post(`/admin/videos/update/${id}`, {}, {
                title,
                length: Math.ceil(lenghtVideo) || length,
                url,
                timestamps
            })
                .then(() => {
                    openNotificationWithIcon('success', 'Chỉnh sửa thành công');
                    setPlay(false)
                    onOk()
                })
                .catch(err => {
                    openNotificationWithIcon('error', 'Chỉnh sửa thất bại ')
                })

        }

    }

    const _handleCancel = () => {
        if (isOkToClose) {
            onCancel();
            setPlay(false);
            setIsOk(true);
            return 1;
        }

        confirm({
            title: 'Thoát khi chưa lưu ?',
            icon: <ExclamationCircleOutlined />,
            content: 'Bạn chưa lưu thông tin thay đổi',
            onOk() {
                onCancel();
                setPlay(false);
                setIsOk(true);
            },
            onCancel() {
                console.log('Cancel');
            },
        });
    }

    const _handleAddNew = () => {
        setPlay(false);
        setIsOk(false);
        const currentTime = parseInt(refVideo.current.getCurrentTime());
        setListInfo({
            ...listInfo,
            [currentTime]: ''
        });
        setTimeout(() => {
            if (document && document.getElementById(currentTime + '')) {
                const elmnt = document.getElementById(currentTime + '');
                elmnt.scrollIntoView();
                // 
                const classElm = document.getElementsByClassName('item-info-video');
                for (let i = 0; i < classElm.length; i++) {
                    classElm[i].style.border = '1px solid #dedede';
                }
                const elmntId = document.getElementsByClassName(currentTime + 'form');
                elmntId[0].style.border = '1px solid #1890ff';
            }
        }, 0)
    };

    const handleSeekTo = (val, index) => {
        setPlayItem(index);
        setPlay(true);
        refVideo.current.seekTo(parseInt(val), 'seconds')
    }

    const _hanldeChangeUrl = () => {
        setTimeout(() => {
            const { url } = formData.getFieldsValue();
            if (url) {
                setUrl(url);
            }
        }, 100)

    }

    return (
        <Modal
            title={"Xem trước video"}
            visible={visible}
            width='95%'
            style={{ top: 20 }}
            onCancel={_handleCancel}
            footer={null}
        >
            <div
                title={"Xem trước video"}
                style={{ display: 'flex', justifyContent: 'space-around' }}
            >
                <div style={{ display: 'flex', alignItems: 'center' }}>
                    <ReactPlayer
                        ref={refVideo}
                        playing={playing}
                        onPlay={() => setPlay(true)}
                        onPause={() => setPlay(false)}
                        controls
                        pip
                        url={urlVideo}
                    />
                </div>
                <Card
                    title={"Video info"}
                    style={{ flex: 1, margin: '0 20px', }}
                    extra={<Button onClick={_handleAddNew} href="#">Thêm mới trích đoạn</Button>}
                >
                    <div style={{ overflowX: 'hidden', overflowY: 'scroll', height: '450px', maxHeight: '500px', scrollBehavior: 'smooth', paddingRight: 15, flex: 1 }}>
                        <Form
                            form={formData}
                            name="control-hooks"
                            layout="vertical"
                        >
                            {
                                formJson.map((item, index) => {
                                    return (
                                        <Form.Item key={String(index)} name={item.name} label={item.label} rules={item.rules} style={item.hiden ? { display: 'none' } : {}} >
                                            <Input placeholder={item.placeHolder} onPaste={item.name == 'url' ? _hanldeChangeUrl : null} onChange={() => setIsOk(false)} />
                                        </Form.Item>
                                    )
                                })
                            }
                        </Form>
                        <Button style={{ float: 'right' }} onClick={_hanldeChangeUrl}> load URL </Button>
                        <Divider orientation="left" plain> Trích đoạn  </Divider>
                        {
                            Object.keys(listInfo).map((key) => <RenderItemDes
                                key={key}
                                handleSeekTo={(val) => { handleSeekTo(val, key) }}
                                item={{ text: listInfo[key], time: key }}
                                isPlay={key === isPlay}
                                handleChangeVal={(val) => {
                                    setIsOk(false)
                                    setListInfo({
                                        ...listInfo,
                                        [key]: val,
                                    })
                                }}
                                _handleRemote={() => {
                                    const {
                                        [key]: dnd,
                                        ...rest
                                    } = listInfo;
                                    setListInfo(rest);
                                }}
                            />)
                        }
                    </div>
                    <div style={{ textAlign: 'right' }}>
                        <Button type="primary" onClick={_handleSave}>Save and close</Button>
                        <Button style={{ marginLeft: 15 }} onClick={_handleReset}>Reset</Button>
                        <Button type="link" onClick={_handleCancel}> Cancel </Button>
                    </div>
                </Card>
            </div>
        </Modal>
    )
});

export default ModalVideo;

// const timestamps = [
//     { text: 'Description 1 video.vietjack.com/backup/old_video/raw-file-bai-4-bt-p1-1563285312.mp4/index.m3u8', time: 0 },
//     { text: 'Description 2', time: 6 },
//     { text: 'Description 3', time: 20 },
//     { text: 'Description 4', time: 30 },
// ];


const RenderItemDes = ({
    item = {},
    handleSeekTo = () => { },
    isPlay = false,
    handleChangeVal,
    _handleRemote = () => { }
}) => {
    return (
        <div
            id={item.time}
            style={{ display: 'flex', alignItems: 'center', padding: '10px 0px' }}
        >
            <div onClick={() => { handleSeekTo(item.time) }}>
                {isPlay ? <PauseCircleOutlined style={{ padding: '0 10px', fontSize: 20 }} /> : <PlayCircleFilled style={{ padding: '0 10px', fontSize: 20 }} />}
            </div>
            <div
                style={{
                    display: 'flex', alignItems: 'center', flex: 1,
                    border: '1px solid #dedede',
                    borderRadius: '7px',
                }}
                className={`item-info-video ${item.time + 'form'}`}
            >
                <TextArea rows={2} value={item.text} style={{ border: 'none' }} onChange={(event) => { handleChangeVal(event.target.value) }} />
                <span style={{ paddingRight: 5, width: 80, textAlign: 'right' }}>{convertTime(item.time)}</span>
            </div>
            <div style={{ padding: 10 }} onClick={_handleRemote}>
                <DeleteFilled />
            </div>
        </div>
    )

}


const convertTime = (second) => {
    const s = second % 60;
    const m = (second - s) / 60;
    return `${m > 9 ? m : '0' + m} : ${s > 9 ? s : '0' + s}`
}


const formJson = [
    {
        name: 'title',
        label: 'Title',
    },
    {
        name: 'url',
        label: 'URL video',
        placeHolder: 'paste url video hoặc nhập url video mới'
    },
]
