
export const gradesFrom = [
    {
        name: 'title',
        label: 'title',
        rules: [{ required: true, min: 3, max: 255 }],
    },
    {
        name: 'description',
        label: 'Description',
        rules: [{ required: true, min: 3, max: 255 }],
    },
    {
        name: 'content',
        label: 'Content',
        rules: [{ min: 3, max: 60000 }],
    },
    {
        name: 'price',
        label: 'Giá (VND)',
        rules: [{ required: true }],
    },
    {
        name: 'lesson_id',
        label: 'lesson_id',
        disabled: true,
        hidden: true
    },
    {
        name: 'id',
        label: 'id',
        disabled: true,
        hidden: true
    },
    {
        name: 'teacher_id',
        label: 'Teacher',
        type: 'select',
        rules: [{ required: true }],
        data: []
    },
    {
        name: 'seo_title',
        label: 'Seo title',
        rules: [{ min: 3, max: 255 }],
    }, ,
    {
        name: 'seo_description',
        label: 'Seo description',
        rules: [{ min: 3, max: 255 }],
    },
    {
        name: 'is_actived',
        label: 'Active',
        type: 'switch'
    }
];


export const columnsGrades = [
    {
        title: 'Title',
        dataIndex: 'title',
        sorter: {
            compare: (a, b) => a.title.localeCompare(b.title),
            multiple: 1,
        },
    },
    {
        title: 'Teacher',
        render: (data) => {
            return (
                <span>{data.teacher.name}</span>
            )
        }
    },
    {
        title: 'Description',
        dataIndex: 'description',
    },
    {
        title: 'Giá (VND)',
        dataIndex: 'price',
        render: (price) => {
            return (
                <span>{formatter.format(price)}</span>
            )
        }
    },
    {
        title: 'view_count',
        dataIndex: 'view_count',
    },
    {
        title: 'bought_count',
        dataIndex: 'bought_count',
    },
    {
        title: 'is_actived',
        dataIndex: 'is_actived',
        render: (is_actived) => {
            return (
                <span>{is_actived?'Active': 'Inactive'}</span>
            )
        }
    },
];

var formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'VND',
  });