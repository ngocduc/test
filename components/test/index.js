
import React, { useEffect, useState, useRef } from 'react';

import { Menu, Row, Col, Button, Input, Spin, Radio, Modal, Form, Select, Table, Space, Card, Tabs, Divider, Switch, InputNumber, notification, Upload } from 'antd';
import { get, isEmpty } from 'lodash';
import {
    DownOutlined, UpOutlined, UploadOutlined
} from '@ant-design/icons';
import MathJax from 'react-mathjax-preview';
import { ReactSortable } from "react-sortablejs";
import request from '../../handler/request';

const { Option } = Select;

import { Editor } from '@tinymce/tinymce-react';

const Test = ({ data }) => {
    const [visibleModal, setVisibleModal] = useState(false);
    const [listTestSuite, setListTestSuite] = useState([]);

    useEffect(() => {
        if (get(data, 'id', '')) requestTest();
    }, [data]);

    const requestTest = () => {
        // lesson id
        const { id: lesson_id = '' } = data;
        if (lesson_id) {
            request.get(`/admin/lessons/${lesson_id}/exams`)
                .then(({ data }) => {
                    setListTestSuite(data.map(i => { i.duration = i.duration / 60; return i }));
                })
                .catch(err => {
                    console.log('err load article', err)
                })
        }
    }

    const handleOk = ({ data = {}, type = "add" }) => {
        requestTest();
        setVisibleModal(false)
    };

    const [showImport, setShowImport] = useState(false)

    return (
        <Row className="site-layout-background" style={{ minHeight: 360 }}>
            {/* table */}
            <Col span={24} style={{ padding: '0 10px' }}>
                <Card title="Biên tập bộ câu hỏi"
                    extra={
                        <div>
                            <Button
                                onClick={() => setShowImport(true)}
                            // loading={isLoading}
                            >
                                Import đề
                            </Button>
                            <Button
                                onClick={() => setVisibleModal({ type: 'add', data: { lesson_id: get(data, 'id', '') } })}
                            // loading={isLoading}
                            >
                                Tạo bộ câu hỏi mới
                            </Button>
                            <Button
                            // onClick={() => setVisibleModal(true)}
                            // loading={isLoading}
                            >
                                reload
                        </Button>

                        </div>
                    }
                >
                    {
                        listTestSuite.map((testSuite, index) => {
                            return (
                                <RenderTestSuite
                                    key={String(index)}
                                    dataTest={testSuite}
                                    _handleEditTestSuite={() => { setVisibleModal({ type: 'edit', data: { ...testSuite, lesson_id: get(data, 'id', '') } }) }}
                                />
                            )
                        })
                    }
                </Card>
                <Divider />
            </Col>
            {/* form */}
            {/* <Col span={4} style={{ padding: '0 10px' }}>
            </Col> */}
            <ModalTestSuite
                visible={visibleModal}
                handleOk={handleOk}
                handleCancel={() => setVisibleModal(false)}
            />
            <ModalImport
                visible={showImport}
                handleOk={() => { setShowImport(false); requestTest() }}
                handleCancel={() => setShowImport(false)}
                lesson_id={get(data, 'id', '')}
            />
        </Row>
    )
}


export default Test;
//  ==================== component

const ModalImport = ({ visible, handleCancel, handleOk, lesson_id }) => {

    const formImport = Form.useForm()[0];
    const formRef = useRef();
    const [loading, setLoading] = useState(false);
    const openNotificationWithIcon = (type, message = '') => notification[type]({ message });

    const onFinishFrom = (values) => {
        if (isNaN(values.duration)) { //valduration duration
            formRef.current.setFields([
                {
                    name: 'duration',
                    errors: ['Thời gian làm bài phải là số'],
                    touched: true
                }
            ])
            return 0;
        }

        if (isNaN(values.curriculum_id)) { //valid duration
            formRef.current.setFields([
                {
                    name: 'id',
                    errors: ['Id bộ đề phải là số'],
                    touched: true
                }
            ])
            return 0;
        }
        setLoading(true);

        request.post('/admin/exams/import-from-course', {}, {
            ...values,
            lesson_id,
            duration: values.duration * 60,
        })
            .then((data) => {
                openNotificationWithIcon('success', 'Tạo bộ đề thành công');
                setTimeout(() => { setLoading(false); formImport.resetFields() }, 200);
                handleOk()
            })
            .catch(err => {
                console.log(err)
                openNotificationWithIcon('error', 'Tạo bộ đề thất bại');
                setTimeout(() => { setLoading(false) }, 200)
            })

        // handle ==== call api
    }

    return (
        <Modal
            title={'Import bộ đề'}
            visible={visible}
            width='60%'
            onCancel={handleCancel}
            footer={null}
        >
            <Card
            // title={typeForm == 'add' ? "Tạo lớp mới mới" : "Chỉnh sửa thông tin lớp"}
            // extra={<a onClick={() => setTypeForm('add')} href="#">Add new</a>}
            >
                <Form
                    ref={formRef}
                    form={formImport}
                    name="control-hooks"
                    onFinish={onFinishFrom}
                    labelCol={{ span: 6 }}
                // layout="vertical"
                >
                    {
                        formJson.map((item, index) => {
                            return (
                                <Form.Item key={String(index)} name={item.name} label={item.label} rules={item.rules} style={item.hiden ? { display: 'none' } : {}} >
                                    <Input />
                                </Form.Item>
                            )
                        })
                    }

                    <Form.Item
                        wrapperCol={{ offset: 6 }}
                    >
                        <Button
                            type="primary"
                            htmlType="submit"
                            style={{ marginRight: '20px' }}
                            loading={loading}
                        >
                            Thêm bộ đề
                        </Button>
                        <Button
                            style={{ marginRight: '20px' }}
                            loading={loading}
                            onClick={() => { formImport.resetFields() }}
                        >
                            Reset
                        </Button>
                    </Form.Item>

                </Form>
            </Card>
        </Modal>

    )
}

const formJson = [
    {
        name: 'title',
        label: 'Title',
        rules: [{ required: true, min: 6 }],
    },
    {
        name: 'curriculum_id',
        label: 'Id bộ đề',
        rules: [{ required: true }],
    },
    {
        name: 'duration',
        label: 'Thời gian làm bài (phút)',
        rules: [{ required: true }],
    },
]

const ModalTestSuite = (props) => {
    const {
        visible,
        handleOk,
        handleCancel,
    } = props;

    const dataFromTestSuite = Form.useForm()[0];
    const dataFromTestSuiteRef = useRef();
    const [active, setActive] = useState({});

    const [loading, setLoading] = useState(false);
    const openNotificationWithIcon = (type, message = '') => notification[type]({ message })

    useEffect(() => {
        if (get(visible, 'type', '') == 'edit') {
            console.log('------------', visible)
            dataFromTestSuite.setFieldsValue(get(visible, 'data', {}))
        } else {
            dataFromTestSuite.resetFields();
        }

    }, [visible])

    const onFinishFromTest = (values) => {
        const {
            duration
        } = values;

        values.is_actived = values.is_actived ? 1 : 0;

        if (isNaN(duration)) { //valid duration
            dataFromTestSuiteRef.current.setFields([
                {
                    name: 'duration',
                    errors: ['Thời gian phải là số'],
                    touched: true
                }
            ])
        } else {
            values.duration = values.duration * 60;
            setLoading(true);
            const type = get(visible, 'type', '');
            const lesson_id = get(visible, 'data.lesson_id', '');
            if (type == 'add') {
                request.post(`/admin/lessons/${lesson_id}/exams`, {}, { ...values, lesson_id })
                    .then(({ response }) => {
                        handleOk({ data: values, type: get(visible, 'type', 'add') });
                        setLoading(false);
                        openNotificationWithIcon('success', 'request thành công');
                    })
                    .catch(err => {
                        console.log('err creact test suite', err)
                        setLoading(false)
                        openNotificationWithIcon('error', 'request thất bại');
                    });
            } else {
                const exam_id = get(visible, 'data.id', '');
                console.log('values---=', values)
                request.post(`/admin/lessons/${lesson_id}/exams/${exam_id}`, {}, { ...values, lesson_id })
                    .then(({ response }) => {
                        handleOk({ data: values, type: 'edit' });
                        setLoading(false)
                        openNotificationWithIcon('success', 'request thành công');
                    })
                    .catch(err => {
                        console.log('err creact test suite', err)
                        setLoading(false)
                        openNotificationWithIcon('error', 'request thất bại');
                    });
            }

        }
        // call api add or edit
    }


    return (
        <Modal
            title={get(visible, 'type', '') == 'add' ? "Tạo bộ câu hỏi" : "Chỉnh sửa bộ câu hỏi"}
            visible={!!visible}
            width='85%'
            style={{ top: 20 }}
            // onOk={handleSubmitTextEdit}
            onCancel={handleCancel}
            footer={null}
        >

            <Card
            // title={typeForm == 'add' ? "Tạo lớp mới mới" : "Chỉnh sửa thông tin lớp"}
            // extra={<a onClick={() => setTypeForm('add')} href="#">Add new</a>}
            >
                <Form
                    ref={dataFromTestSuiteRef}
                    form={dataFromTestSuite}
                    name="control-hooks"
                    onFinish={onFinishFromTest}
                    layout="vertical"
                >
                    {
                        formTestSuite.map((item, index) => {
                            if (!item.type) {
                                return (
                                    <Form.Item key={String(index)} name={item.name} label={item.label} rules={item.rules} style={item.hiden ? { display: 'none' } : {}} >
                                        <Input />
                                    </Form.Item>
                                )
                            } else if (item.type === 'select') {
                                return (
                                    <Form.Item key={String(index)} name={item.name} label={item.label} rules={item.rules} style={item.hiden ? { display: 'none' } : {}} >
                                        <Select
                                            defaultValue={get(item, 'data[0].id', 0)}
                                        >
                                            {
                                                get(item, 'data', [])
                                                    .map((icon) => <Option key={icon.id} value={icon.id}>{icon.title}</Option>)
                                            }
                                        </Select>
                                    </Form.Item>
                                )
                            } else if (item.type === 'switch') {
                                return (
                                    <Form.Item key={String(index)} name={item.name} label={item.label} rules={item.rules} style={item.hiden ? { display: 'none' } : {}} >

                                        <Switch checked={active[item.name]} onChange={() => {
                                            setActive({
                                                [item.name]: !active[item.name]
                                            })
                                        }} />
                                    </Form.Item>
                                )
                            } else if (item.type == 'number') {
                                return (
                                    <Form.Item key={String(index)} name={item.name} label={item.label} rules={item.rules} style={item.hiden ? { display: 'none' } : {}} >
                                        <InputNumber />
                                    </Form.Item>
                                )
                            }
                        })
                    }
                    <Form.Item
                    // wrapperCol={{ offset: 16, span: 8 }}
                    >
                        <Button type="primary" htmlType="submit" style={{ marginRight: '20px' }}
                            loading={loading}
                        >
                            {get(visible, 'type', '') == 'add' ? 'Add new' : 'Update'}
                        </Button>
                    </Form.Item>
                </Form>
            </Card>
        </Modal>
    )
}

// {
//     'title' => 'required|string|min:3|max:255',
//     'seo_title' => 'nullable|string|min:3|max:255',
//     'seo_description' => 'nullable|string|min:3|max:255',
//     'duration' => 'required|integer',
//     'is_actived' => 'boolean',
// }

const formTestSuite = [
    {
        name: 'title',
        label: 'title',
        rules: [{ required: true }],
    },
    {
        name: 'id',
        label: 'id',
        disabled: true,
        hiden: true
    },
    {
        name: 'seo_title',
        label: 'seo_title',
        rules: [],
    },
    {
        name: 'seo_description',
        label: 'seo_description',
        rules: [],
    },
    {
        name: 'duration',
        label: 'Thời gian làm bài (tính theo phút)',
        rules: [{
            required: true,
            message: 'Thời gian làm bài là bắt buôc',
        }],
    },
    {
        name: 'is_active',
        label: 'is_active',
        type: 'switch',
        rules: [],
    },
]


const RenderTestSuite = ({ dataTest, _handleEditTestSuite = () => { } }) => {
    const [expand, setExpand] = useState(false);
    const courseTestRef = useRef();
    const [visibleModal, setVisibleModal] = useState(false);
    const [dataQuestion, setDataQuestion] = useState([]);

    const openNotificationWithIcon = (type, message = '') => {
        notification[type]({ message });
    };

    const handleOk = ({ data = {}, type = "" }) => {
        setExpand(true)
        if (type === 'edit') {
            _getListTest()
        } else { // handle add question
            const newData = dataQuestion.concat(data);
            setDataQuestion(newData);
            if (courseTestRef && courseTestRef.current) {
                setTimeout(() => {
                    courseTestRef.current.scrollIntoView({ behavior: "smooth" });
                    setTimeout(() => {
                        courseTestRef.current.scrollTo({
                            top: 1000000,
                            behavior: 'smooth'
                        })
                    }, 200)
                }, 300)
            }
        }
        setVisibleModal(false);
    };

    const handleCancel = () => {
        setVisibleModal(false)
    };

    const _handleAddQestion = () => {
        setVisibleModal(true);
    }

    useEffect(() => {
    }, [])


    const _getListTest = () => {
        request.get(`/admin/questions?exam_id=${get(dataTest, 'id', '')}&per_page=100`)
            .then(({ data }) => {
                console.log(data, 'data test suite');
                setDataQuestion(data);
            })
            .catch(err => {
                alert(err)
            })
    }

    const _addNewTestSuite = () => {

    }

    const _handleExpand = () => {
        setExpand(!expand);
        if (isEmpty(dataQuestion)) {
            _getListTest()
        }

    }
    const handleOrder = (newState) => {
        setDataQuestion(newState);
    }

    const _handleEditForm = (data) => {
        setVisibleModal({ type: 'edit', data: { ...data, test_id: get(dataTest, 'id', '') } })
    }
    return (
        <div style={{ marginTop: 15 }}>
            <Card title={
                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                    <span>Bộ câu hỏi: {dataTest.title}</span>
                </div>}

                headStyle={{ background: '#dedede' }}
                extra={
                    <div>
                        <Button
                            onClick={() => setVisibleModal({ type: 'add', data: { test_id: get(dataTest, 'id', '') } })}
                        // loading={isLoading}
                        >
                            Thêm câu hỏi
                        </Button>

                        <Button
                            onClick={() => _handleEditTestSuite(dataTest)}
                        // loading={isLoading}
                        >
                            Chỉnh sửa bộ câu hỏi
                        </Button>

                        <Button
                            icon={expand ? <DownOutlined /> : <UpOutlined />}
                            onClick={_handleExpand}
                            style={{ marginLeft: 20 }}
                        />
                    </div>
                }
            >
                {expand ?
                    <div
                        ref={courseTestRef}
                        style={{ height: '90vh', overflow: 'scroll', marginBottom: 20 }}
                    >
                        <ReactSortable list={dataQuestion} setList={handleOrder}>
                            {
                                dataQuestion
                                    .map((testItem, index) => {
                                        return (
                                            <RenderTest
                                                key={String(index)}
                                                data={testItem}
                                                index={index}
                                                handleEditForm={(data) => _handleEditForm({ ...data, index })}
                                            />
                                        )
                                    })
                            }
                        </ReactSortable>
                    </div>
                    : null}
            </Card>
            {/* modal add and edit question */}
            <ModalFormQuestion
                visible={visibleModal}
                handleOk={handleOk}
                handleCancel={handleCancel}
            />
        </div >
    )
}

const RenderTest = ({ data, index, handleEditForm = () => { } }) => {
    const [expand, setExpand] = useState(false);

    return (
        <Card
            title={
                <div style={{ display: 'flex' }}>
                    <span>{`Câu ${index + 1}:  `} &nbsp;</span>
                    <div style={{flex: 1, overflow: 'hidden', marginRight: 10, textOverflow: 'ellipsis', whiteSpace: 'nowrap'}}>
                        <MathJax math={data.content} />
                        {/* <MathJax math={'data.content data.contentdata.contentdata.contentdata.contentdata.contentdata.contentdata.contentdata.contentdata.contentdata.contentdata.contentdata.contentdata.contentdata.content'} /> */}
                    </div>
                </div>
            }
            style={{ marginBottom: 10 }}
            headStyle={{ background: '#efefef' }}
            extra={
                <div>
                    <Button
                        onClick={() => handleEditForm({
                            type: 'edit',
                            data: { ...data, index }
                        })}
                    >
                        Chỉnh sửa
                </Button>
                    <Button
                        icon={expand ? <DownOutlined /> : <UpOutlined />}
                        onClick={() => setExpand(!expand)}
                        style={{ marginLeft: 20 }}
                    />
                </div>
            }
        >
            {
                expand ? <div>
                    <div>
                        <MathJax math={data.content} />
                    </div>
                    <Row>
                        {
                            data.answers.map(item => {
                                return (
                                    <Col span={12} style={{ padding: '10px 20px 10px 2px', float: 'left', display: 'flex', alignItems: 'center', border: '1px solid #f0f0f0' }} key={item.id}>
                                        <Radio checked={item.is_correct} disabled style={{ marginRight: 10 }} />
                                        <MathJax math={item.content} />
                                    </Col>
                                )
                            })
                        }
                        {/* <div style={{ clear: 'both' }} /> */}
                    </Row>

                    <Divider orientation="left" plain>
                        Giải thích
            </Divider>

                    <div>
                        <MathJax math={data.answer_detail} />
                    </div>
                </div> : null
            }
        </Card>
    )
};

const ModalFormQuestion = (props) => {
    const {
        visible,
        handleOk,
        handleCancel,
    } = props;

    const [rightAns, setAnswer] = useState(0);
    // data form
    const [formTest, setStateTest] = useState({});
    const [formAnw, setStateAnw] = useState({});
    const [canEdit, setCanEdit] = useState(false);

    const setDataForm = (newState) => {
        if (canEdit) {
            setStateTest({
                ...formTest,
                ...newState
            })
        }
    };
    // data answer
    const setDataFormAns = (newState) => {
        if (canEdit) {
            setStateAnw({
                ...formAnw,
                ...newState
            })
        }
    };

    useEffect(() => {
        setCanEdit(false);
        const type = get(visible, 'type', '');
        if (type == 'add') {
            setStateTest({
                explain: '',
                question: '',
            });
            setStateAnw({});
        } else if (type == 'edit') {
            const dataInit = get(visible, 'data.data', {});
            const {
                content = '',
                answer_detail = '',
                answers = [],
            } = dataInit;

            setStateTest({
                explain: answer_detail,
                question: content,
            });
            const dataAnswer = answers.reduce((car, cur, index) => {
                if (cur.is_correct) setAnswer(index);
                car[index] = cur.content;
                return car;
            }, {})
            setStateAnw(dataAnswer);
        }
    }, [visible]);

    const [delay, setDelay] = useState(false);

    useEffect(() => {
        setTimeout(() => setDelay(true), 100);
    }, []);

    const openNotificationWithIcon = (type, message = '') => notification[type]({ message })

    const handleSubmitTextEdit = () => {
        const type = get(visible, 'type', 'add');

        const dataInit = get(visible, 'data.data.answers', {});
        const answers = Object.keys(formAnw).map((key, index) => {
            const editInfo = {}
            if (type == 'edit') editInfo.id = get(dataInit, `[${index}]id`, '');
            return {
                content: formAnw[key],
                is_correct: rightAns == index ? 1 : 0,
                ...editInfo
            }
        });

        const dataPost = {
            exam_id: get(visible, 'data.test_id', ''),
            answers: answers,
            content: formTest.question,
            answer_detail: formTest.explain,
        };

        if (type == 'add') {
            request.post(`/admin/questions`, {}, dataPost)
                .then(({ response }) => {
                    handleOk({
                        type: get(visible, 'type', null),
                        data: get(response, 'data.data', {})
                    })
                })
                .catch(err => {
                    if (get(err, 'response.data.errors')) {
                        const dataErr = get(err, 'response.data.errors');
                        if (dataErr.content)
                            openNotificationWithIcon('error', 'Vui lòng điền nội dung câu hỏi')
                        else
                            openNotificationWithIcon('error', 'Vui lòng điền nội dung 4 đáp án')
                    }
                })
        } else { // edit
            const exam_id = get(visible, 'data.data.id', '');
            request.post(`/admin/questions/${exam_id}`, {}, dataPost)
                .then(({ response }) => {
                    handleOk({
                        type: get(visible, 'type', null),
                        data: get(response, 'data.data', {})
                    })
                })
                .catch(err => {
                    if (get(err, 'response.data.errors')) {
                        const dataErr = get(err, 'response.data.errors');
                        if (dataErr.content)
                            openNotificationWithIcon('error', 'Vui lòng điền nội dung câu hỏi')
                        else
                            openNotificationWithIcon('error', 'Vui lòng điền nội dung 4 đáp án')
                    }

                })
        }
    }

    return (
        <Modal
            title="Tạo câu hỏi"
            visible={!!visible}
            width='85%'
            style={{ top: 20 }}
            onOk={handleSubmitTextEdit}
            onCancel={handleCancel}
            footer={[
                <Button key="back" onClick={handleCancel}>
                    Cancel
                </Button>,
                <Button key="submit" type="primary"
                    // loading={loading} 
                    onClick={handleSubmitTextEdit}>
                    Submit
                </Button>,
            ]}
        >
            {delay ?
                <div title="">
                    <p>Câu hỏi: </p>
                    <div style={{ display: 'flex', alignItems: 'center', padding: '10px 0' }}>
                        <EditorCustom
                            height={200}
                            value={get(formTest, 'question', '')}
                            id={`question${get(visible, 'type', 'add') + get(visible, 'data.test_id', '') + get(visible, 'data.data.id', '')}`}
                            onEditorChange={(value) => setDataForm({ 'question': value })}
                            onFocus={() => setCanEdit(true)}
                        />
                    </div>
                    {/* <Divider orientation="left" plain>
                        Các phương án
                    </Divider> */}
                    <div>
                        {
                            [1, 2, 3, 4].map((i, index) => {
                                return (<div style={{ display: 'flex', alignItems: 'center', padding: '10px 0' }} key={String(i)}>
                                    <Radio checked={index === rightAns} onChange={() => { setAnswer(index) }} />
                                    {/* handle edit answer */}
                                    <EditorCustom
                                        value={get(formAnw, index, '')}
                                        id={`answer${i + get(visible, 'type', 'add') + get(visible, 'data.test_id', '') + get(visible, 'data.data.id', '')}`}
                                        onEditorChange={(value) => setDataFormAns({ [index]: value })}
                                        onFocus={() => setCanEdit(true)}
                                    />
                                </div>)
                            })
                        }
                    </div>
                    {/* <Divider orientation="left" plain> Giải thích </Divider> */}
                    <div>
                        <EditorCustom
                            height={200}
                            value={get(formTest, `explain`, '')}
                            id={`explain${get(visible, 'type', 'add') + get(visible, 'data.test_id', '') + get(visible, 'data.data.id', '')}`}
                            onEditorChange={(value) => setDataForm({ "explain": value })}
                            onFocus={() => setCanEdit(true)}
                        />
                    </div>
                </div> : null}
        </Modal>
    )
}


const EditorCustom = ({ value = '', id = '', onEditorChange = () => { }, onFocus = () => { }, height = 100 }) => {
    return (
        <Editor
            id={id}
            value={value}
            onFocus={onFocus}
            init={{
                height,
                menubar: false,
                // menubar: 'file edit insert view format table tools help',
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table paste code help wordcount',
                ],
                external_plugins: {
                    'tiny_mce_wiris': window ? `${window.location.origin}/plugin.min.js` : '',
                },
                toolbar:
                    `image | undo redo | formatselect | bold italic backcolor |  \
                                alignleft aligncenter alignright alignjustify | \
                                fontsizeselect | \
                                tiny_mce_wiris_formulaEditor | tiny_mce_wiris_formulaEditorChemistry`,

                selector: `textarea#${id}`,  // change this value according to your HTML
                fontsize_formats: '11px 12px 14px 16px 18px 24px 36px 48px',
                // font_formats: 'Arial=arial,helvetica,sans-serif; Courier New=courier new,courier,monospace; AkrutiKndPadmini=Akpdmi-n'
                // handle upload image
                images_upload_url: 'https://apps.vietjack.com:8081/admin/images',
                images_upload_handler: function (blobInfo, success, failure) {
                    var xhr, formData;

                    xhr = new XMLHttpRequest();
                    xhr.withCredentials = false;
                    xhr.open('POST', 'https://apps.vietjack.com:8081/admin/images');
                    xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('token'))

                    xhr.onload = function () {
                        var json;

                        if (xhr.status != 200) {
                            failure('HTTP Error: ' + xhr.status);
                            return;
                        }

                        json = JSON.parse(xhr.responseText);

                        if (!json || typeof json.link != 'string') {
                            failure('Invalid JSON: ' + xhr.responseText);
                            return;
                        }

                        success(json.link);
                    };

                    formData = new FormData();
                    formData.append('image', blobInfo.blob(), blobInfo.filename());

                    xhr.send(formData);
                }
            }}
            onEditorChange={onEditorChange}
        />
    )
}