import React, { useEffect, useState, useRef } from 'react';

import { Menu, Row, Col, Button, Input, Spin, Radio, Modal, Form, Select, Table, Space, Card, Tabs, Divider, Switch } from 'antd';
import { get, isEmpty } from 'lodash';
import {
    DownOutlined, UpOutlined, PlusCircleOutlined, EditOutlined, DeleteOutlined
} from '@ant-design/icons';
import MathJax from 'react-mathjax-preview';
import request from '../../handler/request';

import { Editor } from '@tinymce/tinymce-react';

// Document
const Document = ({ data = {} } = {}) => {
    const [listDoc, setListDoc] = useState([]);
    const [visible, setVisible] = useState(false);

    const handleCancel = () => {
        setVisible(false);
    };

    const handleOk = (newValue) => {
        setListDoc(listDoc.concat(newValue));
        setVisible(false);
    }

    useEffect(() => {
        requestDocument()
    }, [data]);

    const requestDocument = () => {
        const { id = '' } = data;
        if (id) {
            request.get(`/admin/lessons/${id}/articles`)
                .then(({ data }) => {
                    console.log('data lesson', data);
                    setListDoc(data)
                })
                .catch(err => {
                    console.log('err load article', err)
                })
        }
    }

    return (
        <div>
            <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                <Button type="primary" onClick={() => setVisible({ type: 'add', idLesson: get(data, 'id', '') })} icon={< PlusCircleOutlined />}> Thêm tài liệu</Button>
                <Button onClick={requestDocument}>Reload</Button>

            </div>
            {/* list */}
            {
                isEmpty(listDoc) ?
                    null :
                    listDoc.map((item, index) => <Categogy key={String(index)} data={item} idLesson={get(data, 'id', '')} handleEdit={requestDocument} />)}
            {/* modal */}
            <ModalCategogyForm
                visible={visible}
                handleOk={handleOk}
                handleCancel={handleCancel}
                idCss="adddoc"
            />
        </div>
    )
}

const Categogy = ({ data, idLesson, handleEdit }) => {
    const [expand, setExpand] = useState(true);
    const [visible, setVisible] = useState(false);

    const handleCancel = () => {
        setVisible(false);
    };

    const handleOk = () => {
        handleEdit();
        setVisible(false);
    }
    return (
        <div style={{ margin: '15px 0px' }}>
            <Card title={get(data, 'title', '')}
                extra={
                    <div>
                        <Button
                            onClick={() => setVisible({ type: 'edit', data, idLesson })}
                            // loading={isLoading}
                            icon={<EditOutlined />}
                        >
                            Chỉnh sửa
                        </Button>
                        <Button
                            icon={expand ? <DownOutlined /> : <UpOutlined />}
                            onClick={() => setExpand(!expand)}
                            style={{ marginLeft: 20 }}
                        />

                    </div>
                }
            >
                {
                    expand ?
                        <div>
                            <MathJax math={get(data, 'content', '')} />
                        </div>
                        : null
                }
            </Card>
            <ModalCategogyForm
                visible={visible}
                handleOk={handleOk}
                handleCancel={handleCancel}
                idCss={`edit${get(data, 'id', '')}`}
            />
        </div>
    )
}


const ModalCategogyForm = (props) => {
    const {
        visible,
        handleOk,
        handleCancel,
        idCss = "editdoc"
    } = props;

    const [content, setContent] = useState('');
    const [dataJsonForm, setDataJsonForm] = useState(bookJsonForm);
    const [dataForm] = Form.useForm();
    const [active, setActive] = useState({});

    useEffect(() => {
        if (visible) {
            const { type = "add", data, idLesson = '' } = visible;

            if (type === 'add') {
                dataForm.resetFields();
                setContent('');
            } else {
                dataForm.setFieldsValue(data);
                setActive({
                    is_actived: Boolean(data.is_actived)
                });
                setContent(get(data, 'content', ''));
            }
        }
    }, [visible]);

    const [delay, setDelay] = useState(false);

    useEffect(() => {
        setTimeout(() => setDelay(true), 100);
    }, []);

    const onFinishFrom = (value) => {
        const { type = "add" } = visible;
        const idLesson = get(visible, 'idLesson', '');
        console.log('valuevalue', value)
        value.is_actived = value.is_actived ? 1 : 0;

        if (type === 'add') {  // add

            request.post(`/admin/lessons/${idLesson}/articles`, {}, { ...value, content })
                .then(({ response = {} } = {}) => {
                    handleOk(get(response, 'data.data', {}));
                })
                .catch(err => {
                    console.log('err create articles', err)
                })

        } else { //edit

            request.post(`/admin/lessons/${idLesson}/articles/${get(value, 'id', '')}`, {}, { ...value, content })
                .then(({ response = {} } = {}) => {
                    handleOk(get(response, 'data.data', {}));
                })
                .catch(err => {
                    console.log('err create articles', err)
                })

        }
    }


    return (
        <Modal
            title="Biên tập tài liệu"
            visible={!!visible}
            width='90%'
            style={{ top: 20 }}
            onCancel={handleCancel}
            footer={null}
        >
            <div style={{ marginBottom: 20 }}>

                <Form
                    // ref={formDataRef}
                    form={dataForm}
                    name="control-hooks"
                    onFinish={onFinishFrom}
                    layout="inline"
                >
                    {
                        dataJsonForm.map((item, index) => {
                            if (item.type === 'select') {
                                return (
                                    <Form.Item key={String(index)} name={item.name} label={item.label} rules={item.rules} style={item.hidden ? { display: 'none' } : { margin: '8px 15px' }} >
                                        <Select
                                            // defaultValue={get(item, 'data[0].id', 0)}
                                            style={{ width: 180 }}
                                        >
                                            {
                                                get(item, 'data', [])
                                                    .map((icon) => <Option key={icon.id} value={icon.id}>{icon.title}</Option>)
                                            }
                                        </Select>
                                    </Form.Item>
                                )
                            } else if (item.type === 'switch') {
                                return (
                                    <Form.Item key={String(index)} name={item.name} label={item.label} rules={item.rules} style={item.hiden ? { display: 'none' } : {}} >
                                        <Switch checked={active[item.name]} onChange={() => {
                                            setActive({
                                                [item.name]: !active[item.name]
                                            })
                                        }} />
                                    </Form.Item>
                                )
                            } else {
                                return (
                                    <Form.Item key={String(index)} name={item.name} label={item.label} rules={item.rules}
                                        style={item.hidden ? { display: 'none' } : { margin: '8px 15px' }}
                                    >
                                        <Input
                                            style={{ width: 180 }}
                                        />
                                    </Form.Item>
                                )
                            }
                        })
                    }
                    <div style={{ width: '100%', marginTop: 15 }}>
                        <span style={{ marginLeft: 10 }}> Nội dung: </span>
                        {delay ?
                            <Editor
                                id={idCss}
                                value={content}
                                init={{
                                    height: '50vh',
                                    menubar: 'file edit insert view format table tools help',
                                    plugins: [
                                        'advlist autolink lists link image charmap print preview anchor',
                                        'searchreplace visualblocks code fullscreen',
                                        'insertdatetime media table paste code help wordcount',
                                    ],
                                    external_plugins: {
                                        'tiny_mce_wiris': window ? `${window.location.origin}/plugin.min.js` : '',
                                    },
                                    toolbar:
                                        `image | undo redo | formatselect | bold italic backcolor |  \
                                        alignleft aligncenter alignright alignjustify | \
                                        fontsizeselect | \
                                        tiny_mce_wiris_formulaEditor | tiny_mce_wiris_formulaEditorChemistry`,

                                    selector: `textarea#${idCss}`,  // change this value according to your HTML
                                    fontsize_formats: '11px 12px 14px 16px 18px 24px 36px 48px',
                                    // handle upload image
                                    images_upload_url: 'https://apps.vietjack.com:8081/admin/images',
                                    images_upload_handler: function (blobInfo, success, failure) {
                                        var xhr, formData;

                                        xhr = new XMLHttpRequest();
                                        xhr.withCredentials = false;
                                        xhr.open('POST', 'https://apps.vietjack.com:8081/admin/images');
                                        xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('token'))

                                        xhr.onload = function () {
                                            var json;

                                            if (xhr.status != 200) {
                                                failure('HTTP Error: ' + xhr.status);
                                                return;
                                            }

                                            json = JSON.parse(xhr.responseText);

                                            if (!json || typeof json.link != 'string') {
                                                failure('Invalid JSON: ' + xhr.responseText);
                                                return;
                                            }

                                            success(json.link);
                                        };

                                        formData = new FormData();
                                        formData.append('image', blobInfo.blob(), blobInfo.filename());

                                        xhr.send(formData);
                                    }
                                    // font_formats: 'Arial=arial,helvetica,sans-serif; Courier New=courier new,courier,monospace; AkrutiKndPadmini=Akpdmi-n'
                                }}
                                onEditorChange={setContent}
                            />
                            : null}
                    </div>
                    <Form.Item                     >
                        <div style={{ marginTop: 15 }}>
                            <Button key="back" onClick={handleCancel} style={{ marginRight: '20px' }}>
                                Cancel
                            </Button>
                            <Button
                                key="submit"
                                type="primary"
                                htmlType="submit"
                            // loading={loading} 
                            // onClick={handleSubmitTextEdit}
                            >
                                Submit
                        </Button>
                        </div>
                    </Form.Item>
                </Form>
            </div>
        </Modal>
    )
}

export default Document;

const bookJsonForm = [
    {
        name: 'title',
        label: 'title',
        rules: [{ required: true, min: 3, max: 255 }],
    },
    {
        name: 'description',
        label: 'description',
        rules: [{ min: 3, max: 255 }],
    },
    // {
    //     name: 'slug',
    //     label: 'slug',
    //     rules: [{ min: 3, max: 255 }],
    // },
    {
        name: 'seo_title',
        label: 'Seo title',
        rules: [{ min: 3, max: 255 }],
    },
    {
        name: 'seo_description',
        label: 'Seo description',
        rules: [{ min: 3, max: 255 }],
    },
    {
        name: 'id',
        label: 'id',
        disabled: true,
        hidden: true
    },
    {
        name: 'is_actived',
        label: 'Active',
        type: 'switch'
    }
]