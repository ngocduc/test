import { Table, Button } from 'antd';

const column = [
    {
        title: 'Name',
        dataIndex: 'name',
        // sorter: {
        //     compare: (a, b) => a.name - b.name,
        //     multiple: 1,
        // },
    },
    {
        title: 'Chinese Score',
        dataIndex: 'chinese',
        sorter: {
            compare: (a, b) => a.chinese - b.chinese,
            multiple: 3,
        },
    },
    {
        title: 'Math Score',
        dataIndex: 'math',
        sorter: {
            compare: (a, b) => a.math - b.math,
            multiple: 2,
        },
    },
    {
        title: 'English Score',
        dataIndex: 'english',
        sorter: {
            compare: (a, b) => a.english.localeCompare(b.english),
            multiple: 1,
        },
    }, {
        title: 'Action',
        key: 'operation',
        fixed: 'right',
        width: 115,
        render: (data) => {
            console.log('data', data)
            return (
                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                    <a> edit </a>
                    <a style={{ color: 'red' }}> delete </a>
                </div>
            )
        }
    },
];

const data = [
    {
        key: '1',
        name: 'John Brown',
        chinese: 98,
        math: 60,
        english: 'a',
    },
    {
        key: '2',
        name: 'Jim Green',
        chinese: 98,
        math: 66,
        english: 'b',
    },
    {
        key: '3',
        name: 'Joe Black',
        chinese: 98,
        math: 90,
        english: 'c',
    },
    {
        key: '4',
        name: 'Jim Red',
        chinese: 88,
        math: 99,
        english: 'd',
    },
    {
        key: '1',
        name: 'John Brown',
        chinese: 98,
        math: 60,
        english: 'a',
    },
    {
        key: '2',
        name: 'Jim Green',
        chinese: 98,
        math: 66,
        english: 'b',
    },
    {
        key: '3',
        name: 'Joe Black',
        chinese: 98,
        math: 90,
        english: 'c',
    },
    {
        key: '4',
        name: 'Jim Red',
        chinese: 88,
        math: 99,
        english: 'd',
    },
    {
        key: '1',
        name: 'John Brown',
        chinese: 98,
        math: 60,
        english: 'a',
    },
    {
        key: '2',
        name: 'Jim Green',
        chinese: 98,
        math: 66,
        english: 'b',
    },
    {
        key: '3',
        name: 'Joe Black',
        chinese: 98,
        math: 90,
        english: 'c',
    },
    {
        key: '4',
        name: 'Jim Red',
        chinese: 88,
        math: 99,
        english: 'd',
    },
    {
        key: '1',
        name: 'John Brown',
        chinese: 98,
        math: 60,
        english: 'a',
    },
    {
        key: '2',
        name: 'Jim Green',
        chinese: 98,
        math: 66,
        english: 'b',
    },
    {
        key: '3',
        name: 'Joe Black',
        chinese: 98,
        math: 90,
        english: 'c',
    },
    {
        key: '4',
        name: 'Jim Red',
        chinese: 88,
        math: 99,
        english: 'd',
    },
    {
        key: '1',
        name: 'John Brown',
        chinese: 98,
        math: 60,
        english: 'a',
    },
    {
        key: '2',
        name: 'Jim Green',
        chinese: 98,
        math: 66,
        english: 'b',
    },
    {
        key: '3',
        name: 'Joe Black',
        chinese: 98,
        math: 90,
        english: 'c',
    },
    {
        key: '4',
        name: 'Jim Red',
        chinese: 88,
        math: 99,
        english: 'd',
    },
    {
        key: '1',
        name: 'John Brown',
        chinese: 98,
        math: 60,
        english: 'a',
    },
    {
        key: '2',
        name: 'Jim Green',
        chinese: 98,
        math: 66,
        english: 'b',
    },
    {
        key: '3',
        name: 'Joe Black',
        chinese: 98,
        math: 90,
        english: 'c',
    },
    {
        key: '4',
        name: 'Jim Red',
        chinese: 88,
        math: 99,
        english: 'd',
    },
    {
        key: '1',
        name: 'John Brown',
        chinese: 98,
        math: 60,
        english: 'a',
    },
    {
        key: '2',
        name: 'Jim Green',
        chinese: 98,
        math: 66,
        english: 'b',
    },
    {
        key: '3',
        name: 'Joe Black',
        chinese: 98,
        math: 90,
        english: 'c',
    },
    {
        key: '4',
        name: 'Jim Red',
        chinese: 88,
        math: 99,
        english: 'd',
    },
    {
        key: '1',
        name: 'John Brown',
        chinese: 98,
        math: 60,
        english: 'a',
    },
    {
        key: '2',
        name: 'Jim Green',
        chinese: 98,
        math: 66,
        english: 'b',
    },
    {
        key: '3',
        name: 'Joe Black',
        chinese: 98,
        math: 90,
        english: 'c',
    },
    {
        key: '4',
        name: 'Jim Red',
        chinese: 88,
        math: 99,
        english: 'd',
    },
    {
        key: '1',
        name: 'John Brown',
        chinese: 98,
        math: 60,
        english: 'a',
    },
    {
        key: '2',
        name: 'Jim Green',
        chinese: 98,
        math: 66,
        english: 'b',
    },
    {
        key: '3',
        name: 'Joe Black',
        chinese: 98,
        math: 90,
        english: 'c',
    },
    {
        key: '4',
        name: 'Jim Red',
        chinese: 88,
        math: 99,
        english: 'd',
    },
    {
        key: '1',
        name: 'John Brown',
        chinese: 98,
        math: 60,
        english: 'a',
    },
    {
        key: '2',
        name: 'Jim Green',
        chinese: 98,
        math: 66,
        english: 'b',
    },
    {
        key: '3',
        name: 'Joe Black',
        chinese: 98,
        math: 90,
        english: 'c',
    },
    {
        key: '4',
        name: 'Jim Red',
        chinese: 88,
        math: 99,
        english: 'd',
    },
    {
        key: '1',
        name: 'John Brown',
        chinese: 98,
        math: 60,
        english: 'a',
    },
    {
        key: '2',
        name: 'Jim Green',
        chinese: 98,
        math: 66,
        english: 'b',
    },
    {
        key: '3',
        name: 'Joe Black',
        chinese: 98,
        math: 90,
        english: 'c',
    },
    {
        key: '4',
        name: 'Jim Red',
        chinese: 88,
        math: 99,
        english: 'd',
    },
];

function onChange(pagination, filters, sorter, extra) {
    // console.log('params', pagination, filters, sorter, extra);
}
const SortTable = ({
    columns = column,
    dataSource = data,
    loading = false
}) => {
    return (
        <Table loading={loading} columns={columns} dataSource={dataSource} onChange={onChange} />
    )
};

export default SortTable;