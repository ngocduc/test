import { useState, useCallback, useEffect } from 'react';

import { Layout, Menu, Row, Col, Button, Input, Spin, Radio, Modal, Form, Select, Table, Space, Card, Drawer } from 'antd';
import { 
    MenuUnfoldOutlined, 
    UserOutlined,
    ReadOutlined,
    ApartmentOutlined,
    FileTextOutlined,
    ProfileOutlined
 } from '@ant-design/icons';
import { useDispatch, useSelector } from 'react-redux';
import Router, { useRouter, withRouter } from 'next/router';

import { setUserInfo } from '../../redux/action/user_info';

const { Header, Content } = Layout;

const LayoutPage = (props) => {
    const dispatch = useDispatch();
    const router = useRouter();

    const [visible, setVisible] = useState(false);
    const [role, setRole] = useState('');

    const showDrawer = () => {
        setVisible(true);
    };
    const onClose = () => {
        setVisible(false);
    };

    const hanldeLogout = () => {
        dispatch(dispatch(setUserInfo({ token: '' })));
        localStorage.setItem('token', '');
        localStorage.setItem('role', '');
        Router.push('/login');
    }

    const _handleClick = () => {
        const userRole = localStorage.getItem('role');
        if (userRole === 'admin') {
            router.push('/user');
        } else {

        }
    }

    useEffect(() => {
        const userRole = localStorage.getItem('role');
        setRole(userRole);
    }, []);

    const state = useSelector(state => state)
    return (
        <div>
            <Layout style={{ minHeight: '100vh' }}>
                <Layout className="site-layout">
                    <Header className="site-layout-background" style={{ padding: 0 }} >
                        <Row>
                            <Col span={2}>
                                <Button type="primary" onClick={showDrawer} style={{ marginBottom: 16 }}>
                                    <MenuUnfoldOutlined />
                                </Button></Col>
                            <Col span={18}>
                            </Col>
                            <Col span={4} style={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'center' }}>
                                <div style={{ marginRight: '10px' }} onClick={_handleClick}>
                                    <UserOutlined style={{ color: '#fff' }} />
                                    <span style={{ color: '#fff' }}>{`  ${role}`}</span>
                                </div>
                                <Button type="primary" onClick={hanldeLogout} style={{ marginRight: '10px' }}>
                                    log out
                                </Button>
                            </Col>
                        </Row>
                    </Header>
                    <Content style={{ margin: '0 16px' }}>
                        {props.children}
                    </Content>
                </Layout>
                <script src="https://wiris.net/demo/plugins/app/WIRISplugins.js?viewer=image"></script>
                <Drawer
                    title="Menu"
                    placement="right"
                    closable={false}
                    onClose={onClose}
                    visible={visible}
                    placement="left"
                    style={{ padding: 0 }}
                >
                    <Menu mode="inline" selectedKeys={props.openKeys} >
                        <Menu.Item key="/grades" icon={<ProfileOutlined />}>
                            <a href="/grades">
                                Quản lý lớp học
                                </a>
                        </Menu.Item>
                        <Menu.Item key="/subject" icon={<FileTextOutlined />}>
                            <a href="/subject">
                                Quản lý môn học
                            </a>
                        </Menu.Item>
                        <Menu.Item key="/book" icon={<ReadOutlined />}>
                            <a href="/book">
                                Quản lý Sách
                        </a>
                        </Menu.Item>
                        {/* menu */}
                        <Menu.Item key="/menu" icon={<ApartmentOutlined />}>
                            <a href="/menu">
                                Quản lý mục lục
                            </a>
                        </Menu.Item>
                        <Menu.Item key="/user" icon={<UserOutlined />}>
                            <a href="/user">
                                Quản lý user
                            </a>
                        </Menu.Item>
                        {/* <Menu.Item key="/lesson" icon={<UserOutlined />}>
                            <a href="/lesson">
                                Quản lý bài học
                            </a>
                        </Menu.Item> */}
                    </Menu>
                </Drawer>
            </Layout>
        </div>
    )
};
export default LayoutPage;
