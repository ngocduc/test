import { combineReducers } from 'redux';
import userInfo from './user_info';

const reducers = combineReducers({
  userInfo,
});
export default reducers;
