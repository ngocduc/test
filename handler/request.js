import axios from 'axios';
import React, { useEffect, useState } from 'react';
import Lottie from 'react-lottie';
import animationData from './1088-shape-types.json';
import styled from 'styled-components';
import Router from 'next/router'

import { setUserInfo } from '../redux/action/user_info';
import Store from '../redux/store';

const request = axios.create({
	// for stg
	baseURL: 'https://apps.vietjack.com:8081',
	// for product
	// baseURL: 'https://apps.vietjack.com:8080/',
	headers: {
		'Content-Type': 'application/json',
		'Accept': 'application/json',
	},
});

const api = {
	get: (url, header) => {
		const { userInfo = {} } = Store.getState() || {};
		const { token = '' } = userInfo;

		const tokenStore = localStorage.getItem('token');
		console.log('url get: ', url)
		return new Promise(function (resolve, reject) {
			request({
				method: 'get',
				url: url,
				headers: {
					...header,
					Authorization: `Bearer ${tokenStore}`
				},
				timeout: 5000,
			})
				.then(response => {
					resolve(response.data);
				})
				.catch(err => {
					if (err.response && err.response.status == '401') {
						alert('token hết hạn vui lòng đăng nhập lại ')
						Router.push('/login')
					}
					console.log('-----------------', err, err.response && err.response.status)
					reject(err);
				})
		});
	},
	post: (url, header, data) => {
		const { userInfo = {} } = Store.getState() || {};

		const tokenStore = localStorage.getItem('token');
		if (tokenStore && !url.includes('login')) header.Authorization = `Bearer ${tokenStore}`;
		console.log('post url:', url)

		if (userInfo.csrf_token || 1) {
			return new Promise(function (resolve, reject) {
				request({
					method: 'post',
					url: url,
					data: data,
					headers: header,
					timeout: 10000,
				})
					.then(response => {
						resolve({ response });
					})
					.catch(error => {
						if (error.response && error.response.status == '419') {
							console.log('reload csrf ==========')
							requestCSRF().then((csrf_token) => {
								request({
									method: 'post',
									url: url,
									data: data,
									headers: {
										...header,
										// 'X-CSRF-TOKEN': csrf_token
									},
									timeout: 5000,
								})
									.then(response => {
										resolve({ response });
									})
									.catch(err => {
										console.log('er', err);
										reject(err);
									})
							})

						} else if (error.response && error.response.status == '401') {
							alert('token hết hạn vui lòng đăng nhập lại ')
							Router.push('/login')
						} else {
							console.log('error post', error);
							reject(error);
						}
					})
			});
		} else {
			return new Promise(function (resolve, reject) {
				requestCSRF().then((csrf_token) => {
					request({
						method: 'post',
						url: url,
						data: data,
						headers: {
							...header,
							// 'X-CSRF-TOKEN': csrf_token
						},
						timeout: 5000,
					})
						.then(response => {
							resolve({ response });
						})
						.catch(err => {
							console.log('er', err);
							reject(err);
						})
				})
			});

		}
	},
	delete: (url, header, data) => {
		return new Promise(function (resolve, reject) {
			request({
				method: 'delete',
				url: url,
				data: data,
				timeout: 5000,
			})
				.then(response => {
					resolve(response);
				})
				.catch(err => {
					reject(err);
				})
		});
	},
	put: (url, header, data) => {
		return new Promise(function (resolve, reject) {
			request({
				method: 'put',
				url: url,
				data: data,
				timeout: 5000,
			})
				.then(response => {
					resolve(response);
				})
				.catch(err => {
					reject(err);
				})
		});
	}
};

export const useRequest = (url, context = [], method = 'get', header, payload) => {
	const [data, setData] = useState(null);
	const [err, setErr] = useState(null);
	let clear = () => { };

	useEffect(() => {
		if (context[0]) {
			api[method](url, header, payload)
				.then(data => {
					setData(data);
				})
				.catch(err => {
					console.log({ err })
					setErr(err);
				})
		}
		return () => {
			clear();
		}
	}, [...context]);
	return [data, data === null && err === null, err];
}


const requestCSRF = () => {
	return api.get('/admin/csrf')
		.then(({ csrf_token }) => {
			Store.dispatch(setUserInfo({ csrf_token }));
			console.log('request csrf_token', csrf_token)
			return csrf_token;
		})
		.catch((err) => {
			console.log('err requesr csrf: ===== ', err)
		})

}

export const Loading = (props) => {
	const { isLoading, err, height } = props;

	const defaultOptions = {
		loop: true,
		autoplay: true,
		animationData: animationData,
		rendererSettings: {
			preserveAspectRatio: 'xMidYMid slice'
		}
	};
	if (err) {
		let message = 'không thể kết nối server';
		if (!netInfo.isConnected) {
			message = "Vui lòng kiểm tra kết nối internet"
		}
		return (
			<div>
				<Lottie options={defaultOptions}
					height={100}
					width={100}
				/>
			</div>
		)
	}
	if (isLoading) {
		console.log({ isLoading })
		return (
			<WrapperLoading height={height}>
				<Lottie options={defaultOptions}
					height={200}
					width={200}
				/>
			</WrapperLoading>
		)
	} else return props.children;
}
const WrapperLoading = styled.div`
	/* height: 100%; */
	width: 100%;
	display: flex;
	justify-content: center;
	align-items: center;
	height: ${({ height }) => height || "100%"};
`
// <LoadingAnimated /> :

export default api;