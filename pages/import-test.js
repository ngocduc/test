import { useState, useCallback, useEffect, useRef } from 'react';

import { Menu, Row, Col, Button, Input, Spin, Radio, Modal, Form, Select, Table, Space, Card, notification } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { useSpring, animated } from 'react-spring'

import styled from 'styled-components';
import { useRouter } from 'next/router'
import { get, constant } from 'lodash';

import request from '../handler/request';
import SearchTable from '../components/share/SearchTable';
// import '../public/styles.css';

const { Option } = Select;

const { Search } = Input;

import Layout from '../components/share/layout';

const App = () => {
    const router = useRouter();
    const [dataClass, setDataClass] = useState([]);
    const [isLoading, setLoading] = useState(false);

    const userInfo = useSelector(state => state.userInfo);
    const [isAdmin, setIsAdmin] = useState(get(userInfo, 'user.role', '') === 'admin');

    useEffect(() => {
        if (get(userInfo, 'user.role', ''))
            setIsAdmin(get(userInfo, 'user.role_name', '') === 'admin')
        else
            setIsAdmin(localStorage.getItem('role') === 'admin')
    }, [userInfo]);

    const openNotificationWithIcon = (type, message = '') => {
        notification[type]({ message });
    };

    useEffect(() => {
        loadDataClass();
    }, []);

    const loadDataClass = (dataSearch) => {
        setLoading(true)
        let search = '';
        if (dataSearch) search = `?keyword=${dataSearch}`
        request.get('/admin/exams/import-from-course' + search)
            .then(({ data }) => {
                setDataClass(data.map(i => ({ ...i, key: i.id })));
                setTimeout(() => setLoading(false), 400)
            })
            .catch(error => {
                console.log('error fetch data grades', error)
                setTimeout(() => setLoading(false), 400)
            })
    }

    return (
        <Layout openKeys={router.pathname}>
            <Row>
                <Col span={12} offset={6} style={{ padding: '0 20px', marginTop: 20 }}>
                    <Search
                        placeholder="input search text"
                        onSearch={value => loadDataClass(value)}
                    // style={{ width: '50%' }}
                    />
                </Col>
            </Row>
            <Row className="site-layout-background" style={{ minHeight: 360, padding: '24px 10px' }}>
                {/* table */}
                <Col span={24} style={{ padding: '0 10px' }}>
                    <Card title="Thông tin bộ đề"
                        extra={
                            <Button onClick={() => loadDataClass()} loading={isLoading}>
                                Làm mới
                            </Button>
                        }
                    >
                        <SearchTable columns={columnsGrades} dataSource={dataClass} loading={isLoading} />
                    </Card>
                </Col>
            </Row>
        </Layout>
    )
};
//
export default App;

const colors = {
    1: '#F18759',
    2: '#A2C1FD',
    3: '#9CCD85',
    'isExpand': '#fff'
}

const colorsHV = {
    1: '#EC6027',
    2: '#87AEFD',
    3: '#61B13D',
    'isExpand': '#fff'
}


// {
//     "id": 1,
//     "user_id": 0,
//     "lesson_id": 2,
//     "exam_id": 0,
//     "curriculum_id": 1015,
//     "title": "Bài 1: Kính yêu Bác Hồ",
//     "duration": 900,
//     "imported_at": null,
//     "status": 0,
//     "message": null,
//     "created_at": "2020-06-26 06:52:47",
//     "updated_at": "2020-06-26 06:52:47",
//     "user": null,
//     "lesson": {
//         "id": 2,
//         "title": "Bài 1: Kính yêu Bác Hồ"
//     }
// }

const columnsGrades = [
    {
        title: 'Title',
        dataIndex: 'title',
        sorter: {
            compare: (a, b) => a.title.localeCompare(b.title),
            multiple: 1,
        },
    },
    {
        title: 'User',
        render: (data) => {
            return (
                <span>{get(data, 'user.name', '')}</span>
            )
        }
    },
    {
        title: 'Lesson',
        render: (data) => {
            return (
                <span>{data.lesson_id}</span>
            )
        }
    },
    {
        title: 'Exam',
        render: (data) => {
            return (
                <span>{data.exam_id}</span>
            )
        }
    },
    {
        title: 'Thời gian',
        render: (data) => {
            return (
                <span>{data.duration / 60}</span>
            )
        }
    },
    {
        title: 'Trạng thái',
        render: (data) => {
            return (
                <span>{sttMap[data.status]}</span>
            )
        }
    },
    {
        title: 'lesson',
        render: (data) => {
            return (
                <span>{get(data, 'lesson.title', '')}</span>
            )
        }
    },
];

const sttMap = {
    0: 'new',
    1: 'success',
    2: 'fail'
}
