import React from 'react'
import App, { Container } from 'next/app'
import withRedux from 'next-redux-wrapper';
import Store from '../redux/store';
import Router from 'next/router'
import Head from 'next/head'


import 'react-sortable-tree/style.css'; // This only needs to be imported once in your app

import '../public/styles.css';

class MyApp extends App {

    static async getInitialProps({ Component, ctx, res }) {
        return {
            pageProps: {
                // Call page-level getInitialProps
                ...(Component.getInitialProps ? await Component.getInitialProps(ctx) : {})
            }
        };

    }

    componentDidMount() {
        console.log('this.props.router.route', this.props.router.route);
        const tokenStore = localStorage.getItem('token');
        const roleStore = localStorage.getItem('role');
        if (!tokenStore || !['admin', 'editor'].includes(roleStore)) Router.push('/login');
        if (this.props.router && this.props.router.route == '/') Router.push('/grades');
    }

    render() {
        //pageProps that were returned  from 'getInitialProps' are stored in the props i.e. pageprops
        const { Component, pageProps, router, store } = this.props;
        return (
            <div>
                <Head>
                    <title>Vietjack</title>
                    <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                </Head>
                {/* <Provider store={store}> */}
                {/* <Layout openKeys={router.pathname}> */}
                <Component {...pageProps} />
                {/* </Layout> */}
                {/* </Provider> */}
            </div>
        )

    }
}
const initStore = () => Store;
export default withRedux(initStore)(MyApp);

