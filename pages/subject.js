import { useState, useCallback, useEffect, useRef } from 'react';

import { Menu, Row, Col, Button, Input, Spin, Radio, Modal, Form, Select, Table, Space, Card, notification } from 'antd';
import { get } from 'lodash';
import styled from 'styled-components';
import { useRouter } from 'next/router';
import { useSpring, animated } from 'react-spring';
import { EditOutlined } from '@ant-design/icons';
import { useDispatch, useSelector } from 'react-redux';

import SearchTable from '../components/share/SearchTable';
import request from '../handler/request';
// import '../public/styles.css';

const { Option } = Select;

const layout = {
    // labelCol: { span: 8 },
    // wrapperCol: { span: 16 },
};
import Layout from '../components/share/layout';

import { useMenuState } from '../handler/globalHook';

const baseApi = 'https://apps.vietjack.com:8080/';


const initListClass = [{ id: '', title: 'Tất cả các lớp' }];
const App = () => {
    const router = useRouter()
    // handle form add/edit
    const formClassRef = useRef();
    const [dataJsonForm, setDataJsonForm] = useState(gradesFrom);

    const [dataFromSelect] = Form.useForm();
    const [dataFromSubject] = Form.useForm();
    // data
    const [listClass, setListClass] = useState(initListClass);
    const [listSubject, setDataSubject] = useState([]);
    const [isLoading, setLoading] = useState(false);

    const [typeForm, setTypeForm] = useState('add');
    // animation 
    const [scale, toggle] = useState(false);
    const { x } = useSpring({
        from: { x: 0 },
        x: scale ? 1 : 0,
        config: { duration: 1000 }
    })
    useEffect(() => {
        if (typeForm == 'add') {
            onResetFormClass();
        }
    }, [typeForm])

    const userInfo = useSelector(state => state.userInfo);
    const [isAdmin, setIsAdmin] = useState(get(userInfo, 'user.role', '') === 'admin');

    useEffect(() => {
        if (get(userInfo, 'user.role', ''))
            setIsAdmin(get(userInfo, 'user.role_name', '') === 'admin')
        else
            setIsAdmin(localStorage.getItem('role') === 'admin')
    }, [userInfo]);

    const openNotificationWithIcon = (type, message = '') => {
        notification[type]({ message });
    };

    // form d
    useEffect(() => {
        dataFromSubject.resetFields();

    }, [listSubject]);

    // load all class
    useEffect(() => {
        setLoading(true);
        request.get('/admin/grades')
            .then(({ data }) => {
                setListClass(initListClass.concat(data));

                const newJsonForm = dataJsonForm.map(i => {
                    if (i.name === 'grade_id') {
                        i.data = data
                    }
                    return i;
                });
                setDataJsonForm(newJsonForm);

                setTimeout(() => {
                    setLoading(false);

                    if (router.query.grade_id) {
                        dataFromSelect.setFieldsValue({ className: +router.query.grade_id });
                    }
                }, 250);
            })
            .catch(err => {
                console.log('err load class ', err);
                setTimeout(() => setLoading(false), 250);
            })
        loadSubject(router.query.grade_id)

        request.get('/admin/masterdata?subject_icons=%7B%7D')
            .then(({ subject_icons = null }) => {
                if (subject_icons) {
                    const newJsonForm = dataJsonForm.map(i => {
                        if (i.name === 'icon_id') {
                            i.data = subject_icons
                        }
                        return i;
                    });
                    setDataJsonForm(newJsonForm);
                }
            })
    }, []);
    const [dataEdit, setDataEdit] = useState({});

    const handleClickEdit = (data) => {
        formClassRef.current.setFieldsValue(data);
        setDataEdit(data);
        setTypeForm('edit');
    }

    const _handleResetForm = () => {
        if (typeForm === 'edit') {
            formClassRef.current.setFieldsValue(dataEdit);
        } else {
            onResetFormClass();
        }
    }

    const handleSelectClass = (value) => {
        loadSubject(value);
    }

    const loadSubject = (id) => {
        setLoading(true);
        let query = '';
        if (id) query += `?grade_id=${id}`
        request.get(`/admin/subjects${query}`)
            .then(({ data }) => {
                setDataSubject(data.map(i => ({ ...i, key: i.id })));
                setTimeout(() => setLoading(false), 250);
            })
            .catch(err => {
                openNotificationWithIcon('error', 'Load danh sách môn thất bại');
                console.log('err load subject with id', id, err)
                setTimeout(() => setLoading(false), 250);
            })
    }

    const handleRefesh = () => {
        const { className = '' } = dataFromSelect.getFieldValue();
        loadSubject(className)
    }

    const columnsClass = [
        ...columnsGrades,
        {
            title: 'Lớp',
            width: 80,
            render: (data) => {
                return (
                    <span>{get(data, 'grade.title')}</span>
                )
            }
        },
        {
            title: 'Action',
            key: 'operation',
            fixed: 'right',
            width: 125,
            render: (data) => {
                return (
                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                        {isAdmin && <a onClick={() => { handleClickEdit(data); toggle(!scale) }}>Edit </a>}
                        {isAdmin && <Line />}
                        <a onClick={() => {
                            router.push({
                                pathname: '/book',
                                query: {
                                    grade_id: get(data, 'grade_id', ''),
                                    subject_id: get(data, 'id', '')
                                }
                            })
                        }}>Book </a>
                    </div>
                )
            }
        }
    ];

    const onFinishFromSubject = (values) => {
        setLoading(true);
        if (typeForm === 'add') {
            request.post('/admin/subjects', {}, values)
                .then((res) => {
                    handleRefesh();
                    openNotificationWithIcon('success', 'Tạo môn thành công');
                    setTimeout(() => setLoading(false), 250);
                })
                .catch(err => {
                    openNotificationWithIcon('error', 'Tạo môn thất bại');
                    console.log('err add new subject', err)
                    setTimeout(() => setLoading(false), 250);
                })
        } else {
            request.post(`/admin/subjects/${values.id}`, {}, values)
                .then((res) => {
                    setTimeout(() => setLoading(false), 250);
                    openNotificationWithIcon('success', 'Chỉnh sửa thành công');
                    handleRefesh();
                    setTypeForm('add');
                })
                .catch(error => {
                    openNotificationWithIcon('error', 'Chỉnh sửa thất bại');
                    setTimeout(() => setLoading(false), 250);
                    if (error.response && error.response.data && error.response.data && error.response.data.errors) {
                        const listErr = get(error, 'response.data.errors', {});
                        console.log('error.response.data', error.response.data.errors)
                        Object.keys(listErr).map((key) => {
                            formClassRef.current.setFields([
                                {
                                    name: key,
                                    errors: listErr[key],
                                },
                            ]);
                        })
                    }
                })
        }



    }
    const onResetFormClass = useCallback(() => {
        if(formClassRef && formClassRef.current && formClassRef.current.resetFields) formClassRef.current.resetFields();
    }, [listSubject]);

    return (

        <Layout openKeys={router.pathname}>
            <Row className="site-layout-background" style={{ minHeight: 360, padding: '24px 10px' }}>
                {/* table */}
                <Col span={isAdmin ? 16 : 24} style={{ padding: '0 10px' }}>
                    <Card title="Dữ liêu môn học"
                        extra={
                            <div>
                                <Form form={dataFromSelect}
                                    layout="inline">
                                    <Form.Item name="className" >
                                        <Select
                                            defaultValue={''}
                                            style={{ width: 180, marginRight: 20 }}
                                            placeholder="Chọn lớp để tiếp tục"
                                            onChange={handleSelectClass}
                                        >
                                            {listClass.map((classItem) => <Option key={classItem.id} value={classItem.id}>{classItem.title}</Option>)}
                                        </Select>
                                    </Form.Item>
                                    <Button
                                        onClick={handleRefesh}
                                        loading={isLoading}
                                    >
                                        Làm mới
                                </Button>
                                </Form>
                            </div>
                        }
                    >
                        <SearchTable columns={columnsClass} dataSource={listSubject} />
                    </Card>
                </Col>
                {/* form */}
                {isAdmin && <Col span={8} style={{ padding: '0 10px' }}>
                    <animated.div
                        style={{
                            transform: x
                                .interpolate({
                                    range: [0, 0.25, 0.35, 0.45, 0.55, 0.65, 0.75, 1],
                                    output: [1, 0.99, 0.98, 1.05, 0.98, 1.05, 1.03, 1]
                                })
                                .interpolate(x => `scale(${x})`)
                        }}>
                        <Card
                            title={typeForm == 'add' ? "Tạo mới môn học" : "Chỉnh sửa môn học"}
                            extra={<a onClick={() => setTypeForm('add')} href="#">Add new</a>}
                            headStyle={{
                                background: typeForm == 'edit' ? "#dedede" : '#fff'
                            }}
                        >
                            <Form
                                ref={formClassRef}
                                form={dataFromSubject}
                                name="control-hooks"
                                onFinish={onFinishFromSubject}
                                layout="vertical"
                            >
                                {
                                    dataJsonForm.map((item = {}, index) => {
                                        if (item.type === 'select') {
                                            return (
                                                <Form.Item key={String(index)} name={item.name} label={item.label} rules={item.rules} style={item.hiden ? { display: 'none' } : {}} >
                                                    <Select
                                                        defaultValue={get(item, 'defaultValue', undefined)}
                                                        placeholder={get(item, 'placeholder', '')}
                                                        disabled={get(item, 'name', '') === 'grade_id' && typeForm == 'edit'}
                                                    >
                                                        {
                                                            get(item, 'data', [])
                                                                .map((icon) => <Option key={icon.id} value={icon.id}>{icon.title}</Option>)
                                                        }
                                                    </Select>
                                                </Form.Item>
                                            )
                                        } else {
                                            return (
                                                <Form.Item key={String(index)} name={item.name} label={item.label} rules={item.rules} style={item.hiden ? { display: 'none' } : {}} >
                                                    <Input />
                                                </Form.Item>
                                            )
                                        }
                                    })
                                }
                                <Form.Item
                                // wrapperCol={{ offset: 16, span: 8 }}
                                >
                                    <Button type="primary" htmlType="submit" style={{ marginRight: '20px' }} loading={isLoading} disabled={isLoading}>
                                        {typeForm == 'add' ? 'Tạo mới' : 'Chỉnh sửa'}
                                    </Button>
                                    <Button onClick={_handleResetForm} style={{ marginRight: '20px' }}>
                                        Reset
                            </Button>
                                </Form.Item>
                            </Form>
                        </Card>
                    </animated.div>
                </Col>
                }
            </Row>
        </Layout>
    )


};
// 
const HandleBtn = styled(Button)`
margin: 0 5px;
&:hover { 
  background-color: ${props => props.hover};
  color: #fff
}

`




const gradesFrom = [
    {
        name: 'title',
        label: 'title',
        rules: [{ required: true, min: 3, message: 'title là bắt buộc và dài hơn 3 ký tự' }],
    },
    {
        name: 'grade_id',
        label: 'Grade',
        rules: [{ required: true, message: ' Grade là bắt buộc' }],
        type: "select",
        data: [],
        placeholder: 'Chọn lớp để tiếp tục'
    },
    {
        name: 'icon_id',
        label: 'Icon',
        type: "select",
        data: [],
        placeholder: 'Chọn icon để tiếp tục',
        rules: [{ required: true, message: 'Icon là bắt buộc' }],
    },
    // {
    //     name: 'slug',
    //     label: 'slug',
    //     rules: [{ min: 3, max: 255 }],
    // },
    {
        name: 'seo_title',
        label: 'seo_title',
        rules: [{ min: 3, max: 255 }],
    },
    {
        name: 'seo_description',
        label: 'seo_description',
        rules: [{ min: 3, max: 255 }],
    },
    {
        name: 'id',
        label: 'id',
        disabled: true,
        hiden: true
    },
]

const treeData = [
    { title: 'Chicken', children: [{ title: 'Egg' }] },
    { title: 'Fish', children: [{ title: 'fingerline' }] }
];

export default App;

const colors = {
    1: '#F18759',
    2: '#A2C1FD',
    3: '#9CCD85',
    'isExpand': '#fff'
}

const colorsHV = {
    1: '#EC6027',
    2: '#87AEFD',
    3: '#61B13D',
    'isExpand': '#fff'
}


const columnsGrades = [
    {
        title: 'Title',
        dataIndex: 'title',
        sorter: {
            compare: (a, b) => a.title.localeCompare(b.title),
            multiple: 1,
        },
    },
    {
        title: 'Icon id',
        dataIndex: 'icon_id',
    },
    {
        title: 'Book count',
        dataIndex: 'books_count',
        sorter: {
            compare: (a, b) => {
                try {
                    return a.books_count - b.books_count;
                } catch (err) {
                    console.log('err', err)
                }
            },
            multiple: 1,
        },
    },
    {
        title: 'Slug',
        dataIndex: 'slug',
    },
    {
        title: 'seo_title',
        dataIndex: 'Seo title',
    },
    {
        title: 'seo_description',
        dataIndex: 'Seo description',
    },
];


const Line = () => (
    <div style={{
        height: '22px',
        width: '1px',
        background: '#bfbfbf',
        margin: '0 5px'
    }} />
)