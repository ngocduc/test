import { useState, useCallback, useEffect, useRef } from 'react';
import { Menu, Row, Col, Button, Input, Spin, Radio, Modal, Form, Select, Table, Breadcrumb, Card } from 'antd';
import {
    PlusOutlined,
    MinusOutlined,
    PlusCircleOutlined,
    EditOutlined,
    DeleteOutlined
} from '@ant-design/icons';
import { get, set, isEmpty } from 'lodash';
import styled from 'styled-components';
import Router, { useRouter, withRouter } from 'next/router';
import { useSelector } from 'react-redux';

import request from '../handler/request';
import SortableTree, { insertNode, addNodeUnderParent } from 'react-sortable-tree';
import Layout from '../components/share/layout';
import { useMenuState } from '../handler/globalHook';

const App = (props) => {
    const router = useRouter();
    const [visibleModal, setVisibleModal] = useState(false);
    const [treeData, setDataTree] = useState([]);
    const [isLoading, setLoading] = useState(false);
    const [breadcrumb, setBreadcrum] = useState([]);
    const userInfo = useSelector(state => state.userInfo);
    const [isAdmin, setIsAdmin] = useState(get(userInfo, 'user.role', '') === 'admin');

    useEffect(() => {
        if (get(userInfo, 'user.role', ''))
            setIsAdmin(get(userInfo, 'user.role_name', '') === 'admin')
        else
            setIsAdmin(localStorage.getItem('role') === 'admin')
    }, [userInfo]);
    // handle modal
    const handleOk = ({ type = "add", newData = {}, dataNode = {} }) => {
        if (type === 'add') {
            const { path } = dataNode;
            const getNodeKey = ({ treeIndex }) => treeIndex;
            const newNodeData = addNodeUnderParent({
                treeData: treeData,
                parentKey: path[path.length - 1],
                expandParent: true,
                getNodeKey,
                newNode: newData,
                addAsFirstChild: false,
            });
            setDataTree(newNodeData.treeData);
            setVisibleModal(false);
        } else { // edit

            const { path } = dataNode;
            const newPath = [...path];
            const pathConvert = newPath.map((item, index) => index ? item - newPath[index - 1] - 1 : 0).join(',children,').split(',');
            const oldData = get(treeData, pathConvert, {});
            const { seo_description, seo_title, slug, title } = newData;
            set(treeData, pathConvert, { ...oldData, seo_description, seo_title, slug, title });
            setDataTree([...treeData]);
            setVisibleModal(false);

        }
    };
    const handleCancel = () => {
        setVisibleModal(false)
    }

    // first load
    useEffect(() => {
        requestTree();
    }, [props.router.query]);

    const updateOrder = () => {
        setLoading(true);
        // console.log(get(treeData, `[0].children`, null), '=')
        if (isEmpty(get(treeData, `[0].children`, null))) {
            setTimeout(() => setLoading(false), 200);
            return 1;
        }
        setLoading(true);
        const dataPost = filterData(get(treeData, `[0].children`, []))
        request.post(`/admin/menus/${get(treeData, '[0].id')}/rebuild-tree`, {}, { children: dataPost })
            .then(data => {
                setTimeout(() => setLoading(false), 200);
            })
            .catch(err => {
                console.log('err', err);
                alert(`update tree err`);
                setTimeout(() => setLoading(false), 200);
            })
    }

    const requestTree = () => {
        try {
            const query = get(props, 'router.query', '');
            if (query && query.id) {
                setLoading(true);
                request.get(`/admin/menus/${query.id}`)
                    .then(({ data, book = {}, grade = {}, subject = {} }) => {
                        data.expanded = true;
                        data.canDrop = false;
                        const pathExpand = get(props, 'router.query.path', '');
                        if (pathExpand) {
                            try {
                                // console.log('pathExpand', pathExpand)
                                const convertPathConvert = pathExpand.split(',');

                                const [, ...pathConvert] = convertPathConvert.map((item, index) => index ? item - convertPathConvert[index - 1] - 1 : 0);

                                const setPath = (input, i) => {
                                    const index = pathConvert[i];
                                    if (input && input.children && input.children[index]) {
                                        console.log(input.children, 'input.children')
                                        input.children[index].expanded = true;
                                        setPath(input.children[index], i + 1)
                                    }
                                }
                                setPath(data, 0);

                                // set(data, pathConvert, {})
                            } catch (err) {
                                console.log('pathExpand', err)
                            }
                        }
                        setDataTree([data]);
                        setTimeout(() => {
                            setLoading(false);

                            setBreadcrum([
                                {
                                    title: get(grade, 'title', ''),
                                    action: () => {
                                        router.push({
                                            pathname: '/subject',
                                            query: {
                                                grade_id: get(grade, 'id', '')
                                            }
                                        })
                                    }
                                },
                                {
                                    title: get(subject, 'title', ''),
                                    action: () => {
                                        router.push({
                                            pathname: '/book',
                                            query: {
                                                grade_id: get(grade, 'id', ''),
                                                subject_id: get(subject, 'id', '')
                                            }
                                        })
                                    }
                                },
                                {
                                    title: get(book, 'title', ''),
                                    action: () => {
                                        router.push({
                                            pathname: '/menu',
                                            query: {
                                                book_id: get(book, 'id', ''),
                                                grade_id: get(grade, 'id', ''),
                                                subject_id: get(subject, 'id', '')
                                            }
                                        })
                                    }
                                },
                                {
                                    title: get(data, 'title', ''),
                                    action: () => { }
                                }
                            ])
                        }, 200);

                        // setTitle(`Mục lục:  ${get(grade, 'title', '')} > ${get(subject, 'title', '')} > ${get(book, 'title', '')}`)
                    })
                    .catch(err => {
                        console.log('err load subject', err);
                        setTimeout(() => setLoading(false), 200);
                    })

            }
        } catch (err) {
            console.log('err', err)
        }
    }

    const handleEventTree = (type, data) => {
        const { node: { id = '' } = {}, path } = data;
        if (type == 'add') {
            setVisibleModal({
                type: 'add',
                data,
            })
        } else if (type == 'edit') {
            setVisibleModal({
                type: 'edit',
                data,
            })
        } else if (type == 'edit_lesson') {
            Router.push({
                pathname: '/lesson',
                query: { id, path: path + '', menu_id: get(props, 'router.query.id', '') },
            })
        } else if (type == 'delete_lesson') {
            console.log(type, data);
        } else {

        }
    }
    const [delay, setDelay] = useState(false);

    useEffect(() => {
        setTimeout(() => setDelay(true), 100);
    }, []);


    return (
        <Layout openKeys={router.pathname}>
            <Row className="site-layout-background" style={{ minHeight: 360, padding: '24px 10px' }}>
                <Col span={24} style={{ padding: '0 10px' }}>
                    <Breadcrumb
                        separator=" > "
                        style={{ marginTop: 10, cursor: 'pointer' }}>
                        {breadcrumb.map(i => {
                            return <Breadcrumb.Item key={i.title} onClick={i.action}>{i.title}</Breadcrumb.Item>
                        })}
                    </Breadcrumb>
                    <Card title={"Mục lục"}
                        extra={
                            <div>
                                {isAdmin && <Button loading={isLoading} onClick={updateOrder}> Lưu thứ thự mới </Button>}
                                <Button loading={isLoading} onClick={requestTree}> Làm mới </Button>
                            </div>
                        }
                    >
                        {
                            delay ?
                                < div style={{ height: '90vh' }}>
                                    <SortableTree
                                        canDrag={isAdmin}
                                        treeData={treeData}
                                        onChange={setDataTree}
                                        generateNodeProps={rowInfo => {
                                            const { path = [], node = {} } = rowInfo;
                                            if (!isAdmin) {
                                                if (isEmpty(get(node, 'children', []))) {
                                                    return {
                                                        buttons: [
                                                            <Button
                                                                size="small"
                                                                type="link"
                                                                shape="circle"
                                                                // icon={<DeleteOutlined />}
                                                                onClick={() => handleEventTree('edit_lesson', rowInfo)}
                                                            > {node.lesson ? 'Edit lesson' : 'Add lesson'} </Button>
                                                        ]
                                                    }
                                                }
                                                else return null
                                            }
                                            // console.log('lessonlessonlesson', node.lesson)
                                            else if (path.length == 1) {
                                                return {
                                                    buttons: [
                                                        <Button
                                                            size="small"
                                                            type="link"
                                                            shape="circle"
                                                            icon={<PlusCircleOutlined />}
                                                            onClick={() => handleEventTree('add', rowInfo)}
                                                        />,
                                                        <Button
                                                            size="small"
                                                            type="link"
                                                            shape="circle"
                                                            style={{ margin: '0 5px' }}
                                                            icon={<EditOutlined />}
                                                            onClick={() => handleEventTree('edit', rowInfo)}
                                                        />
                                                    ]
                                                }
                                            } else if (isEmpty(get(node, 'children', []))) { // lesson
                                                return {
                                                    buttons: [
                                                        <Button
                                                            size="small"
                                                            type="link"
                                                            shape="circle"
                                                            icon={<PlusCircleOutlined />}
                                                            onClick={() => handleEventTree('add', rowInfo)}
                                                        />,
                                                        <Button
                                                            size="small"
                                                            type="link"
                                                            shape="circle"
                                                            style={{ margin: '0 5px' }}
                                                            icon={<EditOutlined />}
                                                            onClick={() => handleEventTree('edit', rowInfo)}
                                                        />,
                                                        <Button
                                                            size="small"
                                                            type="link"
                                                            shape="circle"
                                                            icon={<DeleteOutlined />}
                                                            onClick={() => handleEventTree('delete', rowInfo)}
                                                        />,
                                                        <Line />,
                                                        <Button
                                                            size="small"
                                                            type="link"
                                                            shape="circle"
                                                            // icon={<DeleteOutlined />}
                                                            onClick={() => handleEventTree('edit_lesson', rowInfo)}
                                                        > {node.lesson ? 'Edit lesson' : 'Add lesson'} </Button>,
                                                        // <Line />,
                                                        // <Button
                                                        //     size="small"
                                                        //     type="link"
                                                        //     shape="circle"
                                                        //     // icon={<DeleteOutlined />}
                                                        //     onClick={() => handleEventTree('delete_lesson', rowInfo)}
                                                        // > xoá Lesson </Button>,
                                                    ]
                                                }

                                            } else {
                                                return {
                                                    buttons: [
                                                        <Button
                                                            size="small"
                                                            type="link"
                                                            shape="circle"
                                                            icon={<PlusCircleOutlined />}
                                                            onClick={() => handleEventTree('add', rowInfo)}
                                                        />,
                                                        <Button
                                                            size="small"
                                                            type="link"
                                                            shape="circle"
                                                            style={{ margin: '0 5px' }}
                                                            icon={<EditOutlined />}
                                                            onClick={() => handleEventTree('edit', rowInfo)}
                                                        />,
                                                        <Button
                                                            size="small"
                                                            type="link"
                                                            shape="circle"
                                                            icon={<DeleteOutlined />}
                                                            onClick={() => handleEventTree('delete', rowInfo)}
                                                        />
                                                    ]
                                                }
                                            }
                                        }
                                        }
                                    />
                                </div>
                                : null
                        }
                    </Card>
                </Col>
                {/* form */}
                {/* modal */}

                <ModalMenu
                    visible={visibleModal}
                    handleOk={handleOk}
                    handleCancel={handleCancel}
                />
                {/* end modal */}
            </Row >
        </Layout>
    )
};


const ModalMenu = (props) => {
    const {
        visible,
        handleOk,
        handleCancel,
    } = props;
    const dataFrom = Form.useForm()[0];
    const [isLoading, setLoading] = useState(false);

    useEffect(() => {
        if (get(visible, 'type', '') == 'edit') {
            console.log('------------', visible)
            dataFrom.setFieldsValue(get(visible, 'data.node', {}))
        } else {
            dataFrom.resetFields();
        }

    }, [visible])

    const onFinishFromTest = (values) => {
        const { type = 'add', data = {} } = visible;
        const menuId = get(data, 'node.id', '');
        // request api
        if (type === 'add') {
            setLoading(true)
            request.post(`/admin/menus/${menuId}/insert-children`, {}, values)
                .then(({ response }) => {
                    const dataResponse = get(response, 'data.data', {});
                    handleOk({ type: 'add', newData: dataResponse, dataNode: data });
                    setLoading(false)
                })
                .catch(err => {
                    console.log('err insert node', err);
                    setLoading(false)
                })
        } else { //edit
            setLoading(true)
            request.post(`/admin/menus/${menuId}`, {}, values)
                .then(({ response }) => {
                    const dataResponse = get(response, 'data.data', {});
                    handleOk({ type: 'edit', newData: dataResponse, dataNode: data });
                    setLoading(false)
                })
                .catch(err => {
                    console.log('err insert node', err);
                    setLoading(false)
                })
        }
    }


    return (
        <Modal
            title={get(visible, 'type', 'add') == 'add' ? 'Thêm mục' : 'Chỉnh sửa mục'}
            visible={!!visible}
            width='85%'
            style={{ top: 20 }}
            // onOk={handleSubmitTextEdit}
            onCancel={handleCancel}
            footer={null}
        >
            <Card>
                <Form
                    // ref={formClassRef}
                    form={dataFrom}
                    name="control-hooks"
                    onFinish={onFinishFromTest}
                    layout="vertical"
                >
                    {
                        formTestSuite.map((item, index) => {
                            return (
                                <Form.Item key={String(index)} name={item.name} label={item.label} rules={item.rules} style={item.hiden ? { display: 'none' } : {}} >
                                    <Input />
                                </Form.Item>
                            )
                        })
                    }
                    <Form.Item
                    // wrapperCol={{ offset: 16, span: 8 }}
                    >
                        <Button type="primary" htmlType="submit" style={{ marginRight: '20px' }}
                            loading={isLoading}
                        >
                            {get(visible, 'type', 'add') == 'add' ? 'Add new' : 'Update'}
                        </Button>
                        <Button
                            onClick={handleCancel}
                            style={{ marginRight: '20px' }}
                            loading={isLoading}
                        >
                            Cancel
                        </Button>
                    </Form.Item>
                </Form>
            </Card>
        </Modal>
    )
}

const formTestSuite = [
    {
        name: 'title',
        label: 'title',
        rules: [{ required: true, min: 3, max: 255 }],
    },
    // {
    //     name: 'slug',
    //     label: 'slug',
    //     rules: [{ min: 3, max: 255 }],
    // },
    {
        name: 'seo_title',
        label: 'seo_title',
        rules: [{ min: 3, max: 255 }],
    },
    {
        name: 'seo_description',
        label: 'seo_description',
        rules: [{ min: 3, max: 255 }],
    }
]

export default withRouter(App);

const colors = {
    1: '#F18759',
    2: '#A2C1FD',
    3: '#9CCD85',
    'isExpand': '#fff'
}

const colorsHV = {
    1: '#EC6027',
    2: '#87AEFD',
    3: '#61B13D',
    'isExpand': '#fff'
}

// table content  =====================================================================
const BookMenuContent = ({ bookContent = bookMenu.children }) => {
    const [collapsedBook, setCollapsedBook] = useState(null);
    return (
        <SolidBorder bg={colorsHV[3]}>
            {bookContent.map((book, index) => {
                if (book.type === 0) { // nested
                    return (
                        <BookMenuNested key={String(index)} book={book} index={index} collapsedBook={collapsedBook} setCollapsedBook={setCollapsedBook} />
                    )
                } else { // end
                    return (
                        <Title level={3} text={book.title} key={String(index)} isLast />
                    )
                }
            })}
        </SolidBorder>
    )
}

const BookMenuNested = ({ book, index, collapsedBook, setCollapsedBook }) => {
    return (
        <div>
            <Title
                data={book}
                level={3}
                text={book.title}
                handleClick={() => {
                    setCollapsedBook(collapsedBook === index ? null : index)
                }}
                isExpand={collapsedBook === index}
            />
            {
                collapsedBook === index ? <BookMenuContent bookContent={book.children} /> : null
            }

        </div>

    )
}

const Title = (props) => {
    const actionMenu = useMenuState()[1];
    const {
        text = "",
        handleClick = () => { },
        isLoading = false,
        level = 1,
        isExpand = false,
        isLast,
        type = "",
        data = {},
    } = props;

    const handleOnClickModal = useCallback((formType) => {
        if (data) {
            const {
                _id: parent_id = '',
                title = '',
            } = data;
            actionMenu.setStateMenu({
                type: formType,
                data: {
                    parent_id,
                    title
                }
            })

        }
    }, [data])
    return (
        <TitleWapper>
            <LineBefore color={colorsHV[level]} bg={colors[level]} />
            {
                isLast ? null :
                    <CircleBtn color={colorsHV[level]} bg={isExpand ? colors['isExpand'] : colors[level]} onClick={() => isLoading ? {} : handleClick()} >
                        {isLoading ? <Spin size="small" /> : (isExpand ? <MinusOutlined /> : <PlusOutlined />)}
                    </CircleBtn>
            }
            <p onClick={() => isLoading ? {} : handleClick()} style={{ margin: 0, display: 'inline', color: colorsHV[level], paddingRight: '20px' }}> {text} </p>
            <div>
                {
                    !isLast ?
                        <HandleBtn
                            onClick={() => handleOnClickModal('add')}
                            size="small"
                            type="link"
                            hover='#1A9CFC'
                            shape="circle"
                            style={{ margin: '0 5px' }}
                            // shape="round"
                            icon={<PlusCircleOutlined />}
                        /> : null
                }
                <HandleBtn
                    onClick={() => handleOnClickModal('edit')}
                    style={{ margin: '0 5px' }}
                    size="small"
                    type="link"
                    hover='#1A9CFC'
                    shape="circle"
                    icon={<EditOutlined />}
                />
                <HandleBtn
                    onClick={() => handleOnClickModal('delete')}
                    style={{ margin: '0 5px' }}
                    size="small"
                    type="link"
                    hover='#FD4F54'
                    shape="circle"
                    icon={<DeleteOutlined />}
                />
            </div>
        </TitleWapper>
    )
}

const HandleBtn = styled(Button)`
margin: 0 5px;
&:hover { 
  background-color: ${props => props.hover};
  color: #fff
}

`

const TitleWapper = styled.div`
display: flex;
flex-direction: row;
align-items: center;
`;

const LineBefore = styled.div`
height: 1px;
width: 10px;
background: ${props => props.color};
`;

const CircleBtn = styled.div`
height: 30px;
width: 30px;
background: ${props => props.bg};
border-radius: 30px;
border: ${props => `2px solid ${props.color}`};
margin: 5px;
display: flex;
justify-content: center;
align-items: center;
`;

const SolidBorder = styled.div`
border-left: ${props => `2px solid ${props.bg || colors[1]}`};
margin-left: 29px;
`;

const filterData = (data) => {
    return data.map(i => {
        if (!isEmpty(i.children)) {
            i.children = filterData(i.children)
        }
        return {
            id: i.id,
            children: i.children
        }
    })
};

const Line = () => (
    <div style={{
        height: '100%',
        width: '1px',
        background: '#bfbfbf',
        margin: '0 5px'
    }} />
)