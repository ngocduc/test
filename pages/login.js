import React, { useState } from 'react'
import { Form, Input, Button, Row, Col, notification } from 'antd';
import { get } from 'lodash';
import { useDispatch, useSelector } from 'react-redux';
import { setUserInfo } from '../redux/action/user_info';
import Router from 'next/router'

import request from '../handler/request';

const layout = {
    labelCol: {
        span: 10,
    },
    wrapperCol: {
        span: 14,
    },
};
const tailLayout = {
    wrapperCol: {
        offset: 10,
        span: 14,
    },
};

const Login = (props) => {
    const dispatch = useDispatch()
    const state = useSelector(state => state)
    console.log(state, 123)
    const [loading, setLoading] = useState(false);
    const openNotificationWithIcon = (type, message = '') => notification[type]({ message });

    const onFinish = values => {
        setLoading(true)
        request.post(`/admin/auth/login`, {}, values)
            .then(({ response }) => {
                const userData = get(response, 'data', {});
                console.log('userDatauserData-=', userData)
                dispatch(setUserInfo(userData));
                // props.handleLogin();
                localStorage.setItem('token', userData.token);
                localStorage.setItem('role', userData.user.role_name);

                setTimeout(() => {
                    setLoading(false);
                    Router.push('/grades');
                    openNotificationWithIcon('success', 'Login thành công')
                }, 150)
                // const dataUser = get(response, 'data.user', {});
            })
            .catch(error => {
                setLoading(false);
                console.log('errrrrrrrrrr login', error)
                if (error.response && error.response.data) {
                    const listErr = get(error, 'response.data.errors', {});
                    const err = Object.keys(listErr)
                    console.log(err)
                    openNotificationWithIcon('error', 'Vui lòng kiểm tra lại ' + err[0])
                } else
                    openNotificationWithIcon('error', 'Vui lòng kiểm tra lại thông tin đăng nhập ')

            })
    };

    const onFinishFailed = errorInfo => {
        console.log('Failed:', errorInfo);
    };

    return (
        <Row style={{ marginTop: '20vh' }}>
            <Col span={12} offset={5}>
                <Form
                    {...layout}
                    name="basic"
                    initialValues={{
                        remember: true,
                    }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                >
                    <Form.Item
                        label="Email"
                        name="email"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your username!',
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Password"
                        name="password"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your password!',
                            },
                        ]}
                    >
                        <Input.Password />
                    </Form.Item>

                    <Form.Item {...tailLayout}>
                        <Button type="primary" htmlType="submit">
                            Submit
                        </Button>
                    </Form.Item>
                </Form>
            </Col>
        </Row>
    );
};

export default Login;