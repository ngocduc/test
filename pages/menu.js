import { useState, useCallback, useEffect, useRef } from 'react';

import { Menu, Row, Col, Button, Input, Spin, Radio, Modal, Form, Select, Table, Space, Card } from 'antd';

import styled from 'styled-components';
import Router, { useRouter } from 'next/router'
import { get } from 'lodash';

import request from '../handler/request';
import SearchTable from '../components/share/SearchTable';
// import '../public/styles.css';

const { Option } = Select;

const layout = {
  // labelCol: { span: 8 },
  // wrapperCol: { span: 16 },
};
import Layout from '../components/share/layout';

import { useMenuState } from '../handler/globalHook';

const baseApi = 'https://apps.vietjack.com:8080/';


const initListClass = [{ id: '', title: 'Tất cả các lớp' }];
const initListSubject = [{ id: '', title: 'Tất cả các môn' }];
const initListBook = [{ id: '', title: 'Tất cả các sách' }];



const App = () => {
  const router = useRouter();
  const [dataFilter] = Form.useForm();

  const [menuData, setMenuData] = useState([]);
  const [jsonFilter, setJsonFilter] = useState(initJsonFilter);

  const [typeForm, setTypeForm] = useState('add');
  const [isLoading, setLoading] = useState(false);
  // first load
  useEffect(() => {
    if (!router.query.grade_id) requestDataMenus();
    requestAllgrades(router.query.grade_id);
  }, []);

  const requestAllgrades = (grade_id) => {
    setLoading(true);
    request.get('/admin/grades')
      .then(({ data }) => {

        const newJson = jsonFilter.map((item) => {
          if (item.name === "grades_id") {
            item.data = initListClass.concat(data);
          }
          return item;
        });

        setJsonFilter(newJson);
        setTimeout(() => {
          setLoading(false);
          if (grade_id) {
            // const titleGrade = data.find(i => i.id == grade_id);
            dataFilter.setFieldsValue({ grades_id: +grade_id });
            requestSubject(grade_id, router.query.subject_id)
          }

        }, 200)
      })
      .catch(err => {
        console.log('err load class ', err);
        setTimeout(() => {
          setLoading(false);
        }, 200)
      })
  }

  const handleChangeFilter = (form, value) => {
    if (form === 'grades_id') {
      requestSubject(value);
      requestDataMenus({ grades_id: value });
      dataFilter.setFieldsValue({
        subject_id: '',
        book_id: ''
      });

      const newJson = jsonFilter.map((item) => {
        if (item.name === "book_id") {
          item.data = initListBook;
        }
        return item;
      });

      setJsonFilter(newJson);
    } else if (form == 'subject_id') {
      requestBook(value);
      requestDataMenus({ subject_id: value });
      dataFilter.setFieldsValue({
        book_id: ''
      })
    } else {
      requestDataMenus({ book_id: value })
    }
  };

  const requestSubject = (grades_id, subject_id) => { // => get list subject
    setLoading(true);
    const query = encodeURI(JSON.stringify({
      grade_id: grades_id
    }))
    // get list subject
    request.get(`/admin/masterdata?subjects=${query}`)
      .then(({ subjects }) => {
        const newJson = jsonFilter.map((item) => {
          if (item.name === "subject_id") {
            item.data = initListSubject.concat(subjects);
          }
          return item;
        });

        setJsonFilter(newJson);
        // call book and set subject

        setTimeout(() => {
          setLoading(false);
          if (subject_id) {
            // const titleSubject = subjects.find(i => i.id == subject_id);
            dataFilter.setFieldsValue({ subject_id: +subject_id });
            requestBook(router.query.subject_id, router.query.book_id)
          }

        }, 200)
      })
      .catch(err => {
        setLoading(false);
        // setTimeout(() => setLoading(false), 250);
        console.log('err load subject', err)
      })
  }

  const requestBook = (subject_id, book_id) => { // => get list subject
    const query = encodeURI(JSON.stringify({
      subject_id: subject_id
    }))
    // get list subject
    request.get(`/admin/masterdata?books=${query}`)
      .then(({ books }) => {
        const newJson = jsonFilter.map((item) => {
          if (item.name === "book_id") {
            item.data = initListBook.concat(books);
          }
          return item;
        });

        setJsonFilter(newJson);
        // call menu and set book

        setTimeout(() => {
          setLoading(false);
          if (book_id) {
            // const titleBook = books.find(i => i.id == book_id);
            dataFilter.setFieldsValue({ book_id: +book_id });
            requestDataMenus({ book_id })
          }

        }, 200)
      })
      .catch(err => {
        // setTimeout(() => setLoading(false), 250);
        console.log('err load subject', err)
      })
  }


  const handleClickEdit = (data) => {
    setTypeForm('edit');

    Router.push({
      pathname: '/table-content',
      query: { id: data.id },
    })
  }

  const columnsClass = [
    ...columnsGrades,
    {
      title: 'Grade',
      render: (data) => {
        return (
          <span>{get(data, 'book.subject.grade.title', '')}</span>
        )
      }
    },
    {
      title: 'Subject',
      render: (data) => {
        return (
          <span>{get(data, 'book.subject.title', '')}</span>
        )
      }
    },
    {
      title: 'Book',
      render: (data) => {
        return (
          <span>{get(data, 'book.title', '')}</span>
        )
      }
    },
    {
      title: 'Action',
      key: 'operation',
      fixed: 'right',
      width: 115,
      render: (data) => {
        return (
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <a onClick={() => handleClickEdit(data)}> Edit </a>
            {/* <a> delete </a> */}
          </div>
        )
      }
    }
  ];


  // new layout

  const requestDataMenus = (dataFilterConvert = {}) => {
    // const dataFormFilter = dataFilter.getFieldsValue();
    // const { grades_id, subject_id, book_id } = dataFormFilter;
    // const dataFilterConvert = {
    //   grades_id, subject_id, book_id, ...props
    // }

    let query = '';
    if (dataFilterConvert.book_id) query += `book_id=${dataFilterConvert.book_id}`;
    else if (dataFilterConvert.subject_id) query += `subject_id=${dataFilterConvert.subject_id}`;
    else if (dataFilterConvert.grades_id) query += `grade_id=${dataFilterConvert.grades_id}`;

    setLoading(true);

    request.get(`/admin/menus?${query}`)
      .then(({ data }) => {
        setMenuData(data.map(i => ({ ...i, key: i.id })));
        setTimeout(() => setLoading(false), 400)
      })
      .catch(error => {
        console.log('error fetch data grades', error)
        setTimeout(() => setLoading(false), 400)
      })
    // Call
  }


  return (
    <Layout openKeys={router.pathname}>

      <Row className="site-layout-background" style={{ minHeight: 360, padding: '24px 10px' }}>{/* table */}
        {/* table */}
        <Col span={24} style={{ padding: '0 10px' }}>
          <Card title="Menu"
            extra={
              <div style={{ display: 'flex' }}>
                <Form form={dataFilter} layout="inline">
                  {
                    jsonFilter.map((itemFilter, index) => {
                      return (
                        <Form.Item
                          key={String(index)}
                          name={itemFilter.name}
                          label={itemFilter.label}
                          // rules={itemFilter.rules}
                          // style={itemFilter.hidden ? { display: 'none' } : {}}
                          placeholder={itemFilter.placeholder}
                        >
                          <Select
                            style={{ width: 150, marginRight: 20 }}
                            defaultValue={''}
                            onChange={(value) => handleChangeFilter(itemFilter.name, value)}
                          // defaultValue={get(item, 'data[0].id', 0)}
                          >
                            {
                              get(itemFilter, 'data', [])
                                .map((icon) => <Option key={icon.id} value={icon.id}>{icon.title}</Option>)
                            }
                          </Select>
                        </Form.Item>
                      )
                    })
                  }
                </Form>
                <Button onClick={() => requestDataMenus()} loading={isLoading}>
                  Làm mới
              </Button>
              </div>
            }
          >
            <SearchTable columns={columnsClass} dataSource={menuData} />
          </Card>
        </Col>
      </Row>
    </Layout>
  )


};
// 
const HandleBtn = styled(Button)`
margin: 0 5px;
&:hover { 
  background-color: ${props => props.hover};
  color: #fff
}
`

const initJsonFilter = [
  {
    name: 'grades_id',
    label: null,
    placeholder: 'Chọn lớp',
    data: initListClass,
  },
  {
    name: 'subject_id',
    label: null,
    placeholder: 'Chọn môn',
    data: initListSubject,
  },
  {
    name: 'book_id',
    label: null,
    placeholder: 'Chọn sách',
    data: initListBook,
  },

];

const menuJson = [
  {
    name: 'title',
    label: 'title',
    rules: [{ required: true }],
  },
  {
    name: 'id',
    label: 'id',
    disabled: true,
    hiden: true
  },
  {
    name: 'seo_title',
    label: 'seo_title',
    rules: [],
  },
  {
    name: 'seo_description',
    label: 'seo_description',
    rules: [],
  }
];


export default App;

const columnsGrades = [
  {
    title: 'Title',
    dataIndex: 'title',
    sorter: {
      compare: (a, b) => {
        try {
          return a.title.localeCompare(b.title)
        } catch (err) {
          console.log('err', err)
        }
      },
      multiple: 1,
    },
  },
  {
    title: 'Slug',
    dataIndex: 'slug',
  },
  {
    title: 'Seo title',
    dataIndex: 'seo_title',
  },
];
