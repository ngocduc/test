import { useState, useCallback, useEffect, useRef } from 'react';

import { Menu, Row, Col, Button, Input, Spin, Radio, Modal, Form, Select, Table, Space, Card, notification } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { useSpring, animated } from 'react-spring'

import styled from 'styled-components';
import { useRouter } from 'next/router'
import { get, constant } from 'lodash';

import request from '../handler/request';
import SearchTable from '../components/share/SearchTable';
// import '../public/styles.css';

const { Option } = Select;

import Layout from '../components/share/layout';

const App = () => {
  const router = useRouter();
  const formClassRef = useRef();
  const [dataFromClass] = Form.useForm();

  const [dataClass, setDataClass] = useState([]);
  const [typeForm, setTypeForm] = useState('add');
  const [isLoading, setLoading] = useState(false);
  const [dataEdit, setDataEdit] = useState({});

  const userInfo = useSelector(state => state.userInfo);
  const [isAdmin, setIsAdmin] = useState(get(userInfo, 'user.role', '') === 'admin');

  useEffect(() => {
    if (get(userInfo, 'user.role', ''))
      setIsAdmin(get(userInfo, 'user.role_name', '') === 'admin')
    else
      setIsAdmin(localStorage.getItem('role') === 'admin')
  }, [userInfo]);

  const openNotificationWithIcon = (type, message = '') => {
    notification[type]({ message });
  };

  const handleClickEdit = (data) => {
    formClassRef.current.setFieldsValue(data);
    setDataEdit(data);
    setTypeForm('edit')
  }

  const _handleResetForm = () => {
    if (typeForm === 'edit') {
      formClassRef.current.setFieldsValue(dataEdit);
    } else {
      onResetFormClass();
    }
  }

  const columnsClass = [
    ...columnsGrades,
    {
      title: 'Action',
      key: 'operation',
      fixed: 'right',
      width: 130,
      render: (data) => {
        return (
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            {isAdmin && <a onClick={() => { handleClickEdit(data); toggle(!scale) }}> Edit </a>}
            {isAdmin && <Line />}
            <a onClick={() => {
              router.push({
                pathname: '/subject',
                query: {
                  grade_id: get(data, 'id', '')
                }
              })
            }}>Subject</a>
            {/* <a> delete </a> */}
          </div>
        )
      }
    }
  ];

  const onFinishFromClass = (values) => {
    setLoading(true)
    if (typeForm == 'add') {
      values.key = new Date().getTime();

      request.post('/admin/grades', {}, values)
        .then(({ response = {} } = {}) => {
          const newData = [...dataClass, get(response, 'data.data', values)];
          openNotificationWithIcon('success', 'Tạo lớp thành công');
          setDataClass(newData);
          onResetFormClass();
        })
        .catch(error => {
          openNotificationWithIcon('error', 'Tạo lớp thất bại');
          console.log('error creact grades', error);
          if (error.response && error.response.data) {
            const listErr = get(error, 'response.data.errors', {});
            console.log('error.response.data', error.response.data.errors)
            Object.keys(listErr).map((key) => {
              formClassRef.current.setFields([
                {
                  name: key,
                  errors: listErr[key],
                },
              ]);
            })
          }
        })
    } else {
      console.log('edit cccc', values)
      request.post(`/admin/grades/${values.id}`, {}, values)
        .then((dataCreate) => {
          openNotificationWithIcon('success', 'Chỉnh sửa lớp thành công');
          loadDataClass();
          onResetFormClass();
          setTypeForm('add');
        })
        .catch(error => {
          openNotificationWithIcon('error', 'Chỉnh sửa lớp thất bại');
          console.log('error creact grades', error);
          if (error.response && error.response.data && error.response.data && error.response.data.errors) {
            const listErr = get(error, 'response.data.errors', {});
            console.log('error.response.data', error.response.data.errors)
            Object.keys(listErr).map((key) => {
              formClassRef.current.setFields([
                {
                  name: key,
                  errors: listErr[key],
                },
              ]);
            })
          }
        })
    }
    setTimeout(() => setLoading(false), 400)

  }
  const onResetFormClass = useCallback(() => {
    formClassRef.current.resetFields();
  }, []);


  // new layout

  useEffect(() => {
    loadDataClass();
  }, []);

  const loadDataClass = () => {
    setLoading(true)
    request.get('/admin/grades')
      .then(({ data }) => {
        setDataClass(data.map(i => ({ ...i, key: i.id })));
        setTimeout(() => setLoading(false), 400)
      })
      .catch(error => {
        console.log('error fetch data grades', error)
        setTimeout(() => setLoading(false), 400)
      })
    // Call
  }
  // animation 
  const [scale, toggle] = useState(false)
  const { x } = useSpring({
    from: { x: 0 },
    x: scale ? 1 : 0,
    config: { duration: 1000 }
  })


  return (
    <Layout openKeys={router.pathname}>
      <Row className="site-layout-background" style={{ minHeight: 360, padding: '24px 10px' }}>
        {/* table */}
        <Col span={isAdmin ? 16 : 24} style={{ padding: '0 10px' }}>
          <Card title="Thông tin lớp học"
            extra={
              <Button onClick={() => loadDataClass()} loading={isLoading}>
                Làm mới
              </Button>}
          >
            <SearchTable columns={columnsClass} dataSource={dataClass} />
          </Card>
        </Col>
        {/* form */}
        {isAdmin && <Col span={8} style={{ padding: '0 10px' }}>
          <animated.div
            style={{
              transform: x
                .interpolate({
                  range: [0, 0.25, 0.35, 0.45, 0.55, 0.65, 0.75, 1],
                  output: [1, 0.99, 0.98, 1.05, 0.98, 1.05, 1.03, 1]
                })
                .interpolate(x => `scale(${x})`)
            }}>
            <Card
              title={typeForm == 'add' ? "Tạo lớp mới mới" : "Chỉnh sửa thông tin lớp"}
              extra={<a onClick={() => { setTypeForm('add'); onResetFormClass() }} href="#"> Thêm mới</a>}
            >
              <Form
                ref={formClassRef}
                form={dataFromClass}
                name="control-hooks"
                onFinish={onFinishFromClass}
                layout="vertical"
              >
                {
                  gradesFrom.map((item, index) => {
                    return (
                      <Form.Item key={String(index)} name={item.name} label={item.label} rules={item.rules} style={item.hiden ? { display: 'none' } : {}} >
                        <Input disabled={item.disabled || false} />
                      </Form.Item>
                    )
                  })
                }
                <Form.Item
                // wrapperCol={{ offset: 16, span: 8 }}
                >
                  <Button type="primary" htmlType="submit" style={{ marginRight: '20px' }} loading={isLoading}>
                    {typeForm == 'add' ? 'Tạo mới' : 'Chỉnh sửa'}
                  </Button>
                  <Button onClick={_handleResetForm} style={{ marginRight: '20px' }}>
                    Reset
                </Button>
                </Form.Item>
              </Form>
            </Card>
          </animated.div>
        </Col>}
      </Row>
    </Layout>
  )


};
// 
const HandleBtn = styled(Button)`
margin: 0 5px;
&:hover { 
  background-color: ${props => props.hover};
  color: #fff
}
`

const gradesFrom = [
  {
    name: 'title',
    label: 'title',
    rules: [{ required: true, min: 3, max: 255 }],
  },
  {
    name: 'id',
    label: 'id',
    disabled: true,
    hiden: true
  },
  // {
  //   name: 'slug',
  //   label: 'slug',
  //   rules: [{ min: 3, max: 255 }],
  // },
  {
    name: 'seo_title',
    label: 'Seo title',
    rules: [{ min: 3, max: 255 }],
  },
  {
    name: 'seo_description',
    label: 'Seo description',
    rules: [{ min: 3, max: 255 }],
  }
]
const fullModal = {
  grades: {
    form: gradesFrom,
    title: ''
  }
}

const treeData = [
  { title: 'Chicken', children: [{ title: 'Egg' }] },
  { title: 'Fish', children: [{ title: 'fingerline' }] }
];

export default App;

const colors = {
  1: '#F18759',
  2: '#A2C1FD',
  3: '#9CCD85',
  'isExpand': '#fff'
}

const colorsHV = {
  1: '#EC6027',
  2: '#87AEFD',
  3: '#61B13D',
  'isExpand': '#fff'
}


const columnsGrades = [
  {
    title: 'Title',
    dataIndex: 'title',
    sorter: {
      compare: (a, b) => a.title.localeCompare(b.title),
      multiple: 1,
    },
  },
  {
    title: 'Seo title',
    dataIndex: 'seo_title',
    // sorter: {
    //   compare: (a, b) => a.seo_title.localeCompare(b.seo_title),
    //   multiple: 2,
    // },
  },
  {
    title: 'Seo description',
    dataIndex: 'seo_description',
    // sorter: {
    //   compare: (a, b) => a.seo_description.localeCompare(b.seo_description),
    //   multiple: 1,
    // },
  },
];


const Line = () => (
  <div style={{
    height: '22px',
    width: '1px',
    background: '#bfbfbf',
    margin: '0 5px'
  }} />
)