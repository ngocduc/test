import { useState, useCallback, useEffect, useRef } from 'react';
import { Menu, Row, Col, Button, Input, Spin, Radio, Modal, Form, Select, Table, Space, Card, notification, Switch } from 'antd';
import {
    EditOutlined,
    RightOutlined,
} from '@ant-design/icons';
import { useSpring, animated } from 'react-spring';
import { useDispatch, useSelector } from 'react-redux';
import { get } from 'lodash';
import styled from 'styled-components';
import Router, { useRouter } from 'next/router';

import SearchTable from '../components/share/SearchTable';
import request from '../handler/request';

const { Option } = Select;
import Layout from '../components/share/layout';


const initListClass = [{ id: '', title: 'Tất cả các lớp' }];
const initListSubject = [{ id: '', title: 'Tất cả các môn' }];
const App = () => {
    const router = useRouter()
    const [dataJsonForm, setDataJsonForm] = useState(bookJsonForm);
    const formDataRef = useRef();
    const [dataFormFilter] = Form.useForm();
    const [dataForm] = Form.useForm();
    // for filter
    const [listClass, setListClass] = useState(initListClass);
    const [listSubject, setDataSubject] = useState(initListSubject);
    // for form data

    const [glass_form, set_glass_form] = useState([]);
    const [subject_form, set_subject_form] = useState([]);
    // 
    const [listBook, setBookData] = useState([])

    const [isLoading, setLoading] = useState(false);
    const [typeForm, setTypeForm] = useState('add');

    const [active, setActive] = useState({});

    const userInfo = useSelector(state => state.userInfo);
    const [isAdmin, setIsAdmin] = useState(get(userInfo, 'user.role', '') === 'admin');

    useEffect(() => {
        if (get(userInfo, 'user.role', ''))
            setIsAdmin(get(userInfo, 'user.role_name', '') === 'admin')
        else
            setIsAdmin(localStorage.getItem('role') === 'admin')
    }, [userInfo]);

    // animation 
    const [scale, toggle] = useState(false)
    const { x } = useSpring({
        from: { x: 0 },
        x: scale ? 1 : 0,
        config: { duration: 1000 }
    })


    const openNotificationWithIcon = (type, message = '') => notification[type]({ message })

    // first load
    useEffect(() => {
        setLoading(true);
        // load class
        request.get('/admin/grades')
            .then(({ data }) => {
                set_glass_form(data);
                setListClass(initListClass.concat(data));
                setTimeout(() => {
                    setLoading(false);
                    if (router.query.grade_id) {
                        dataFormFilter.setFieldsValue({ className: +router.query.grade_id });
                        handleSelectClass(router.query.grade_id, router.query.subject_id)
                    }
                }, 250);
            })
            .catch(err => {
                console.log('err load class ', err);
                setTimeout(() => setLoading(false), 250);
            })
        // load all book
        if (!router.query.grade_id && !router.query.subject_id)
            requestBooks();


        request.get('/admin/masterdata?subject_icons=%7B%7D')
            .then(({ subject_icons = null }) => {
                if (subject_icons) {
                    const newJsonForm = dataJsonForm.map(i => {
                        if (i.name === 'icon_id') {
                            i.data = subject_icons
                        }
                        return i;
                    });
                    setDataJsonForm(newJsonForm);
                }
            })

    }, []);

    const requestBooks = ({ grade_id = '', subject_id = '' } = {}) => {
        let query = '';
        if (subject_id) query += `subject_id=${subject_id}`;
        else if (grade_id) query += `grade_id=${grade_id}`;
        setLoading(true);
        request.get(`/admin/books?${query}`)
            .then(({ data = [] }) => {
                const dataBook = data.map(i => {
                    const subjectName = get(i, 'subject.title', '');
                    const gradesName = get(i, 'subject.grade.title', '');
                    const menu = get(i, 'menu_item.id', '');
                    return {
                        ...i,
                        key: i.id,
                        subject: subjectName,
                        grades: gradesName,
                        grade_id: gradesName,
                        subject_id_hidden: get(i, 'subject_id', ''),
                        subject_id: subjectName,
                        menu
                    };
                });
                setBookData(dataBook);
                setTimeout(() => setLoading(false), 250);
            })
            .catch(err => {
                console.log('err load class ', err);
                setTimeout(() => setLoading(false), 250);
            })
    }

    const handleRefesh = () => {
        const { className = '', subject = '' } = dataFormFilter.getFieldsValue();
        requestBooks({ subject_id: subject, grade_id: className })
    }

    const handleSelectClass = (gradeId, subject_id = '') => { // => get list subject
        dataFormFilter.setFieldsValue({ subject: '' });
        const query = encodeURI(JSON.stringify({
            grade_id: gradeId
        }))
        // get list subject
        request.get(`/admin/masterdata?subjects=${query}`)
            .then(({ subjects }) => {
                console.log('data subject', subjects)
                setDataSubject(initListSubject.concat(subjects));
                // setDataSubject(data.map(i => ({ ...i, key: i.id })));
                setTimeout(() => {
                    setLoading(false);
                    if (subject_id) {
                        dataFormFilter.setFieldsValue({ subject: +subject_id });
                        handleSelectSubject(subject_id)
                    }
                }, 250);
            })
            .catch(err => {
                setTimeout(() => setLoading(false), 250);
            })
        // get list book in class
        if (!subject_id) {
            requestBooks({ grade_id: gradeId })
        }
    }

    const handleSelectSubject = (value) => {
        const { className = '', subject = '' } = dataFormFilter.getFieldsValue();
        if (value)
            requestBooks({ subject_id: value })
        else {
            requestBooks({ grade_id: className })

        }
    }

    const [dataEdit, setDataEdit] = useState({});
    const handleClickEdit = (data) => {
        // formDataRef.current.setFieldsValue(data);
        dataForm.setFieldsValue(data);
        setActive({
            is_actived: Boolean(data.is_actived)
        });
        setDataEdit(data);
        setTypeForm('edit')
    }



    const _handleResetForm = () => {
        if (typeForm === 'edit') {
            formClassRef.current.setFieldsValue(dataEdit);
            setActive({
                is_actived: Boolean(dataEdit.is_actived)
            });
        } else {
            onResetForm();
        }
    }

    const columnsClass = [
        ...columnsGrades,
        {
            title: 'Action',
            key: 'operation',
            fixed: 'right',
            width: 120,
            render: (data) => {
                return (
                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                        {isAdmin && <a onClick={() => { handleClickEdit(data); toggle(!scale) }}> Edit </a>}
                        {isAdmin && <Line />}
                        <a onClick={() => handleShowMenu(data)}> Menu </a>
                    </div>
                )
            }
        }
    ];

    const handleShowMenu = (data) => {
        Router.push({
            pathname: '/table-content',
            query: { id: data.menu },
        })
        console.log('datadata', data)
    }

    const onFinishFromClass = (valueForm) => {
        valueForm.is_actived = valueForm.is_actived ? 1 : 0;
        const activeProp = Object.keys(active).reduce((car, cur) => {
            car[cur] = active[cur] ? 1 : 0;
            return car;
        }, {});
        const values = {
            ...valueForm,
            ...activeProp
        }
        if (typeForm === 'add') {
            request.post(`/admin/books`, {}, values)
                .then(() => {
                    // setTimeout(() => setLoading(false), 250);
                    // alert('create book success')
                    handleRefesh();

                    openNotificationWithIcon('success', 'Tạo sách thành công');
                })
                .catch(error => {

                    openNotificationWithIcon('error', 'Tạo lớp thất bại');
                    setTimeout(() => setLoading(false), 250);
                    if (error.response && error.response.data) {
                        const listErr = get(error, 'response.data.errors', {});
                        console.log('listErrlistErr', listErr)
                        Object.keys(listErr).map((key) => {
                            formDataRef.current.setFields([
                                {
                                    name: key,
                                    errors: listErr[key],
                                },
                            ]);
                        })
                    }
                })
        } else {
            const {
                grade_id,
                subject_id,
                id,
                ...restValue
            } = values;
            // console.log('subject_id', subject_id, get(values, 'subject_id_hidden', ''))
            request.post(`/admin/books/${id}`, {}, {
                ...restValue,
                subject_id: get(values, 'subject_id_hidden', '')
            })
                .then(() => {
                    // setTimeout(() => setLoading(false), 250);
                    // alert('create book success')
                    handleRefesh();
                    setTypeForm('add');
                    openNotificationWithIcon('success', 'Chỉnh sửa sách thành công');
                })
                .catch(error => {
                    setTimeout(() => setLoading(false), 250);
                    openNotificationWithIcon('error', 'Chỉnh sửa sách thất bại');
                    if (error.response && error.response.data) {
                        const listErr = get(error, 'response.data.errors', {});
                        // console.log('listErrlistErr', listErr)
                        Object.keys(listErr).map((key) => {
                            formDataRef.current.setFields([
                                {
                                    name: key,
                                    errors: listErr[key],
                                },
                            ]);
                        })
                    }
                })
        }

    }
    const onResetForm = useCallback(() => {
        formDataRef.current.resetFields();
    }, []);


    // handle Form 
    const onChangeClassForm = (value) => {
        const query = encodeURI(JSON.stringify({
            grade_id: value
        }))
        // get list subject
        request.get(`/admin/masterdata?subjects=${query}`)
            .then(({ subjects }) => {
                set_subject_form(subjects);
                // setDataSubject(data.map(i => ({ ...i, key: i.id })));
                setTimeout(() => setLoading(false), 250);
            })
            .catch(err => {
                setTimeout(() => setLoading(false), 250);
            })

    }

    useEffect(() => {
        if (typeForm == 'add' && formDataRef && formDataRef.current && formDataRef.current.resetFields)
            formDataRef.current.resetFields();
    }, [typeForm])

    useEffect(() => {
        if (formDataRef && formDataRef.current && formDataRef.current.resetFields) formDataRef.current.resetFields();
    }, [listBook]);


    return (
        <Layout openKeys={router.pathname}>
            <Row className="site-layout-background" style={{ minHeight: 360, padding: '24px 10px' }}>
                {/* table */}
                <Col span={isAdmin ? 17 : 24} style={{ padding: '0 10px' }}>
                    <Card title="Dữ liêu sách"
                        extra={
                            <div>
                                <Form form={dataFormFilter}
                                    layout="inline">
                                    <Form.Item name="className" >
                                        <Select
                                            defaultValue={''}
                                            style={{ width: 180, marginRight: 20 }}
                                            placeholder="Chọn lớp để tiếp tục"
                                            onChange={val => handleSelectClass(val)}
                                        >
                                            {listClass.map((classItem) => <Option key={classItem.id} value={classItem.id}>{classItem.title}</Option>)}
                                        </Select>
                                    </Form.Item>
                                    {/*  */}
                                    <Form.Item name="subject" >
                                        <Select
                                            defaultValue={''}
                                            style={{ width: 180, marginRight: 20 }}
                                            placeholder="Chọn môn để tiếp tục"
                                            onChange={val => handleSelectSubject(val)}
                                        >
                                            {listSubject.map(({ id = '', title = '' }) => <Option key={id} value={id}>{title}</Option>)}
                                        </Select>
                                    </Form.Item>

                                    <Button
                                        onClick={handleRefesh}
                                        loading={isLoading}
                                    >
                                        Làm mới
                                    </Button>
                                </Form>
                            </div>
                        }
                    >
                        <SearchTable columns={columnsClass} dataSource={listBook} />
                    </Card>
                </Col>
                {/* form */}
                {isAdmin && <Col span={7} style={{ padding: '0 10px' }}>
                    <animated.div
                        style={{
                            transform: x
                                .interpolate({
                                    range: [0, 0.25, 0.35, 0.45, 0.55, 0.65, 0.75, 1],
                                    output: [1, 0.99, 0.98, 1.05, 0.98, 1.05, 1.03, 1]
                                })
                                .interpolate(x => `scale(${x})`)
                        }}>
                        <Card title={typeForm == 'add' ? "Thêm mới sách" : "Chỉnh sửa sách"}
                            extra={<a onClick={() => setTypeForm('add')} href="#">Thêm mới</a>}
                        >
                            <Form
                                ref={formDataRef}
                                form={dataForm}
                                name="control-hooks"
                                onFinish={onFinishFromClass}
                                layout="vertical"
                            >
                                <Form.Item name="grade_id" label={'Grades'} rules={[{ required: true }]} >
                                    <Select
                                        disabled={typeForm == 'edit'}
                                        placeholder="Chọn lớp để tiếp tục"
                                        onChange={onChangeClassForm}
                                    >
                                        {
                                            glass_form
                                                .map((icon) => <Option key={icon.id} value={icon.id}>{icon.title}</Option>)
                                        }
                                    </Select>
                                </Form.Item>
                                <Form.Item name="subject_id" label={'Subject'} rules={[{ required: true }]} >
                                    <Select
                                        disabled={typeForm == 'edit'}
                                        placeholder="Chọn môn để tiếp tục"
                                    >
                                        {
                                            subject_form
                                                .map((icon) => <Option key={icon.id} value={icon.id}>{icon.title}</Option>)
                                        }
                                    </Select>
                                </Form.Item>
                                {
                                    dataJsonForm.map((item, index) => {
                                        if (item.type === 'select') {
                                            return (
                                                <Form.Item key={String(index)} name={item.name} label={item.label} rules={item.rules} style={item.hidden ? { display: 'none' } : {}} >
                                                    <Select
                                                    // defaultValue={get(item, 'data[0].id', 0)}
                                                    >
                                                        {
                                                            get(item, 'data', [])
                                                                .map((icon) => <Option key={icon.id} value={icon.id}>{icon.title}</Option>)
                                                        }
                                                    </Select>
                                                </Form.Item>
                                            )
                                        } else if (item.type === 'switch') {
                                            return (
                                                <Form.Item
                                                    key={String(index)}
                                                    valuePropName={item.name}
                                                    name={item.name}
                                                    label={item.label}
                                                    rules={item.rules}
                                                    style={item.hiden ? { display: 'none' } : {}}
                                                >
                                                    <Switch checked={active[item.name]} onChange={() => {
                                                        setActive({
                                                            [item.name]: !active[item.name]
                                                        })
                                                    }} />
                                                    {/* {getFieldDecorator('name', {
                                                initialValue: true,
                                                valuePropName: 'checked'
                                            })(<Switch />)} */}
                                                    {/* <Form.Item name={item.name} noStyle><Switch /></Form.Item> */}
                                                    {/* {getFieldDecorator(item.name, {})(<Switch />)} */}
                                                </Form.Item>
                                            )
                                        } else {
                                            return (
                                                <Form.Item key={String(index)} name={item.name} label={item.label} rules={item.rules}
                                                    style={item.hidden ? { display: 'none' } : {}}
                                                >
                                                    <Input />
                                                </Form.Item>
                                            )
                                        }
                                    })
                                }
                                <Form.Item
                                // wrapperCol={{ offset: 16, span: 8 }}
                                >
                                    <Button type="primary" htmlType="submit" style={{ marginRight: '20px' }}>
                                        {typeForm == 'add' ? 'Thêm mới' : 'Chỉnh sửa'}
                                    </Button>
                                    <Button onClick={_handleResetForm} style={{ marginRight: '20px' }}>
                                        Reset
                            </Button>
                                </Form.Item>
                            </Form>
                        </Card>
                    </animated.div>
                </Col>
                }
            </Row>
        </Layout>
    )


};
// 
const HandleBtn = styled(Button)`
margin: 0 5px;
&:hover { 
  background-color: ${props => props.hover};
  color: #fff
}

`




const bookJsonForm = [
    {
        name: 'title',
        label: 'title',
        rules: [{ required: true }],
    },
    {
        name: 'icon_id',
        label: 'Icon',
        rules: [{ required: true }],
        type: "select",
        data: [],
    },
    {
        name: 'is_actived',
        label: 'Active',
        type: 'switch'
    },
    {
        name: 'seo_title',
        label: 'Seo title',
        rules: [],
    },
    {
        name: 'seo_description',
        label: 'Seo description',
        rules: [],
    },
    {
        name: 'id',
        label: 'id',
        disabled: true,
        hidden: true
    },
    {
        name: 'subject_id_hidden',
        label: 'subject_id_hidden',
        disabled: true,
        hidden: true
    }
]
export default App;

const columnsGrades = [
    {
        title: 'Title',
        dataIndex: 'title',
        sorter: {
            compare: (a, b) => a.title.localeCompare(b.title),
            multiple: 1,
        },
    },
    {
        title: 'Icon',
        dataIndex: 'icon_id',
    },
    {
        title: 'Lớp',
        dataIndex: 'grades',
    },
    {
        title: 'Môn',
        dataIndex: 'subject',
    },
    {
        title: 'Slug',
        dataIndex: 'slug',
    },
    {
        title: 'Seo title',
        dataIndex: 'seo_title',
    },
    {
        title: 'Active',
        render: (data) => {
            return (
                <span>{data.is_actived ? 'active' : 'inactive'}</span>
            )
        }
    },
];


const Line = () => (
    <div style={{
        height: '22px',
        width: '1px',
        background: '#bfbfbf',
        margin: '0 5px'
    }} />
)