import { useState, useCallback, useEffect, useRef } from 'react';
import { Menu, Row, Col, Button, Input, Spin, Radio, Modal, Form, Select, message, Switch, Card, notification, Upload } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import { useRouter } from 'next/router'
import { get } from 'lodash';
import { useSpring, animated } from 'react-spring';

import request from '../handler/request';
import SearchTable from '../components/share/SearchTable';
import Layout from '../components/share/layout';

const { Option } = Select;

const App = () => {
    const router = useRouter();
    const formRef = useRef();
    const [dataFromUser] = Form.useForm();
    const [userData, setDataUser] = useState([]);
    const [typeForm, setTypeForm] = useState('add');
    const [isLoading, setLoading] = useState(false);
    const [active, setActive] = useState({});
    const [dataEdit, setDataEdit] = useState(defaultValueForm);
    // 
    const [userFrom] = useFormData(userFromInit);
    const [columnsClass, setColumnsData] = useState(columnsGrades);
    const [file, setAvatar] = useState();
    useEffect(() => dataFromUser.resetFields(), [dataEdit]);
    // first load
    useEffect(() => {
        if (localStorage.getItem('role') === 'admin') {
            const userRole = localStorage.getItem('role');
            if (userRole !== 'admin') {
                router.push('/login');
            }
            // load all user
            _loadUser();
        } else {
            alert("Vui lòng đăng nhập tài khoản Admin để tiếp tục")
            router.push('/login')
        }
        // set class data
    }, []);

    const openNotificationWithIcon = (type, message = '') => {
        notification[type]({ message });
    };

    const handleClickEdit = (data) => {
        // formRef.current.setFieldsValue(data);
        setActive({
            is_actived: Boolean(data.is_actived)
        });
        setDataEdit(data);
        setTypeForm('edit')
    }

    const _handleResetForm = () => {
        if (typeForm === 'edit') {
            formRef.current.setFieldsValue(dataEdit);
        } else {
            onResetForm();
        }
    }

    const onFinishFromClass = (values) => {
        setLoading(true);
        values.is_actived = values.is_actived ? 1 : 0;
        values.key = new Date().getTime();
        const formData = new FormData();
        console.log('valuesvaluesvaluesvalues', values)
        Object.keys(values).map((keyItem) => {
            if(keyItem === 'grade_id' && values[keyItem] === undefined ) return 1;
            if (keyItem !== 'avatar_')
                formData.append(keyItem, values[keyItem])
        });
        if (file) {
            formData.append('avatar', file);
        }

        if (typeForm == 'add') {
            request.post('/admin/users', { 'Content-Type': 'multipart/form-data' }, formData)
                .then(({ response = {} } = {}) => {
                    openNotificationWithIcon('success', 'Tạo user thành công');
                    onResetForm();
                    _loadUser();
                    setLoading(false)

                })
                .catch(error => {
                    openNotificationWithIcon('error', 'Tạo user thất bại');
                    console.log('error creact grades', error);
                    if (error.response && error.response.data) {
                        const listErr = get(error, 'response.data.errors', {});
                        console.log('error.response.data', error.response.data.errors)
                        Object.keys(listErr).map((key) => {
                            formRef.current.setFields([
                                {
                                    name: key,
                                    errors: listErr[key],
                                },
                            ]);
                        })
                    }
                })
        } else {
            request.post(`/admin/users/${values.id}`, { 'Content-Type': 'multipart/form-data' }, formData)
                .then(() => {
                    openNotificationWithIcon('success', 'Chỉnh sửa user thành công');
                    _loadUser();
                    onResetForm();
                    setTypeForm('add');
                })
                .catch(error => {
                    openNotificationWithIcon('error', 'Chỉnh sửa user thất bại');
                    console.log('error creact grades', error);
                    if (error.response && error.response.data && error.response.data && error.response.data.errors) {
                        const listErr = get(error, 'response.data.errors', {});
                        console.log('error.response.data', error.response.data.errors)
                        Object.keys(listErr).map((key) => {
                            formRef.current.setFields([
                                {
                                    name: key,
                                    errors: listErr[key],
                                },
                            ]);
                        })
                    }
                })
        }
        setTimeout(() => setLoading(false), 400)

    }
    const onResetForm = useCallback(() => {
        setDataEdit(defaultValueForm)
        formRef.current.resetFields();
    }, []);

    const _loadUser = () => {
        setLoading(true)
        request.get('/admin/users')
            .then(({ data }) => {
                setDataUser(data.map(i => ({ ...i, key: i.id })));
                setTimeout(() => setLoading(false), 400)
            })
            .catch(error => {
                console.log('error fetch data grades', error)
                setTimeout(() => setLoading(false), 400)
            })
        // Call
    }
    // animation 
    const [scale, toggle] = useState(false);
    const { x } = useSpring({
        from: { x: 0 },
        x: scale ? 1 : 0,
        config: { duration: 1000 }
    });

    useEffect(() => {
        // set data table
        setColumnsData([
            ...columnsGrades,
            {
                title: 'Action',
                key: 'operation',
                fixed: 'right',
                width: 115,
                render: (data) => {
                    return (
                        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                            <a onClick={() => { handleClickEdit(data); toggle(!scale) }}> edit </a>
                            {/* <a> delete </a> */}
                        </div>
                    )
                }
            }
        ]);

    }, [scale, columnsGrades])


    return (
        <Layout openKeys={router.pathname}>
            <Row className="site-layout-background" style={{ minHeight: 360, padding: '24px 10px' }}>
                {/* table */}
                <Col span={17} style={{ padding: '0 10px' }}>
                    <Card title="Thông tin user"
                        extra={
                            <Button onClick={() => _loadUser()} loading={isLoading}>
                                Làm mới
                            </Button>
                        }
                    >
                        <SearchTable columns={columnsClass} dataSource={userData} />
                    </Card>
                </Col>
                {/* form */}
                <Col span={7} style={{ padding: '0 10px' }}>

                    <animated.div
                        style={{
                            transform: x
                                .interpolate({
                                    range: [0, 0.25, 0.35, 0.45, 0.55, 0.65, 0.75, 1],
                                    output: [1, 0.99, 0.98, 1.05, 0.98, 1.05, 1.03, 1]
                                })
                                .interpolate(x => `scale(${x})`)
                        }}>
                        <Card title={typeForm == 'add' ? "Tạo user mới" : "Chỉnh sửa thông tin user"}
                            extra={<a onClick={() => { setTypeForm('add'); onResetForm() }} href="#">Add new</a>}
                        >
                            <Form
                                ref={formRef}
                                form={dataFromUser}
                                name="control-hooks"
                                onFinish={onFinishFromClass}
                                layout="vertical"
                                initialValues={dataEdit}
                            >
                                {
                                    userFrom.map((item, index) => {
                                        if (item.type === 'select') {
                                            return (
                                                <Form.Item
                                                    key={String(index)}
                                                    name={item.name}
                                                    label={item.label}
                                                    rules={item.rules}
                                                    style={item.hidden ? { display: 'none' } : {}}
                                                >
                                                    <Select>
                                                        {
                                                            get(item, 'data', [])
                                                                .map((item) => <Option key={item.id} value={item.id}>{item.title}</Option>)
                                                        }
                                                    </Select>
                                                </Form.Item>
                                            )
                                        } else if (item.type === 'switch') {
                                            return (
                                                <Form.Item
                                                    key={String(index)}
                                                    valuePropName={item.name}
                                                    name={item.name}
                                                    label={item.label}
                                                    rules={item.rules}
                                                    style={item.hiden ? { display: 'none' } : {}}
                                                >
                                                    <Switch checked={active[item.name]} onChange={() => {
                                                        setActive({
                                                            [item.name]: !active[item.name]
                                                        })
                                                    }} />
                                                </Form.Item>
                                            )
                                        } else if (item.type === 'password') {
                                            return (
                                                <Form.Item key={String(index)} name={item.name} label={item.label} rules={[{ required: typeForm === 'add' ? true : false }]} style={item.hiden ? { display: 'none' } : {}} >
                                                    <Input.Password />
                                                </Form.Item>
                                            )
                                        } else {
                                            return (
                                                <Form.Item key={String(index)} name={item.name} label={item.label} rules={item.rules} style={item.hiden ? { display: 'none' } : {}} >
                                                    <Input disabled={item.disabled || false} />
                                                </Form.Item>
                                            )
                                        }
                                    })
                                }
                                <Form.Item
                                    name="avatar_"
                                    label="Avatar"
                                    valuePropName="fileList"
                                    getValueFromEvent={normFile}
                                >
                                    <Upload
                                        name="avatar"
                                        listType='picture'
                                        fileList={[file]}
                                        beforeUpload={fileData => {
                                            try {
                                                if (fileData && fileData.type) {
                                                    const isJpgOrPng = fileData.type === 'image/jpeg' || fileData.type === 'image/png';
                                                    if (!isJpgOrPng) {
                                                        message.error('You can only upload JPG/PNG file!');
                                                        return false;
                                                    } else {
                                                        setAvatar(fileData);
                                                    }

                                                }
                                                return false;
                                            } catch (err) {
                                                console.log('errr', err)
                                                return false;
                                            }
                                        }}
                                    >
                                        <Button>
                                            <UploadOutlined /> Click to upload
                                        </Button>
                                    </Upload>
                                </Form.Item>
                                <Form.Item>
                                    <Button type="primary" htmlType="submit" style={{ marginRight: '20px' }} loading={isLoading}>
                                        {typeForm == 'add' ? 'Add new' : 'Update'}
                                    </Button>
                                    <Button onClick={_handleResetForm} style={{ marginRight: '20px' }}>
                                        Reset
                                </Button>
                                </Form.Item>
                            </Form>
                        </Card>
                    </animated.div>
                </Col>
            </Row>
        </Layout>
    )
};


export default App;

const normFile = e => {
    console.log('Upload event:', e);
    if (Array.isArray(e)) {
        return e;
    }
    return e && e.fileList;
};

const columnsGrades = [
    {
        title: 'Email',
        dataIndex: 'email',
        sorter: {
            compare: (a, b) => a.email.localeCompare(b.email),
            multiple: 1,
        },
    },
    {
        title: 'name',
        dataIndex: 'name',
    },
    {
        title: 'Role',
        // dataIndex: 'role',
        render: (data) => {
            return <span>{roleConvert[data.role]}</span>
        }
    },
    {
        title: 'Actived',
        render: (data) => {
            return <span>{data.is_actived ? 'active' : 'inactive'}</span>
        }
    },
    {
        title: 'phone',
        dataIndex: 'phone',
    },
    {
        title: 'gender',
        // dataIndex: 'gender',
        render: (data) => {
            return <span>{genderConvert[data.gender]}</span>
        }
    },
];

// {
//     'name' => 'required|string|min:3|max:255',
//     'email' => 'required|email',
//     'password' => 'nullable|string|min:8|max:255',
//     'phone' => 'nullable|string|min:8|max:255',
//     'gender' => 'integer|in:0,1,2',
//     'grade_id' => 'nullable|integer|exists:grades,id',
//     'role' => 'integer|in:1,2,3',
//     'is_actived' => 'integer|boolean',
// }

const userFromInit = [
    {
        name: 'email',
        label: 'Email',
        rules: [{ required: true, min: 3, max: 255, type: 'email' }],
    },
    {
        name: 'name',
        label: 'Name',
        rules: [{ required: true, min: 3, max: 255 }],
    },
    {
        name: 'password',
        label: 'Password',
        rules: [{ min: 8, max: 255 }],
        type: 'password'
    },
    {
        name: 'is_actived',
        label: 'Active',
        type: 'switch'
    },
    {
        name: 'id',
        label: 'id',
        disabled: true,
        hiden: true
    },
    {
        name: 'phone',
        label: 'Phone',
        rules: [{
            min: 8, max: 255,
            // pattern: new RegExp('/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$', 'im')
        }],
    },
    {
        name: 'grade_id',
        label: 'Lớp',
        type: 'select',
        data: [],
    },
    {
        name: 'gender',
        label: 'Giới tính',
        type: 'select',
        data: [{
            id: 0,
            title: 'Nữ',
        }, {
            id: 1,
            title: 'Nam',
        }, {
            id: 2,
            title: 'Khác',
        }],
        rules: [{ required: true }],
    },
    {
        name: 'role',
        label: 'Role',
        type: 'select',
        data: [{
            id: 1,
            title: 'admin',
        }, {
            id: 2,
            title: 'editor',
        }, {
            id: 3,
            title: 'normal',
        }, {
            id: 4,
            title: 'Teacher',
        }],
        rules: [{ required: true }],
    }
    // role: 1 - admin, 2 - editor, 3 - normal
]

const roleConvert = {
    1: 'admin',
    2: 'editor',
    3: 'normal',
};
const genderConvert = {
    0: 'Nữ',
    1: 'Nam',
    2: 'Khác',
}

const defaultValueForm = {
    role: 3,
    gender: 2,
}

const useFormData = (initForm) => {
    const [formData, setFormData] = useState(initForm);
    useEffect(() => {
        request.get('/admin/grades')
            .then(({ data }) => {
                const newJsonForm = formData.map(i => {
                    if (i.name === 'grade_id') {
                        i.data = data
                    }
                    return i;
                });
                setFormData(newJsonForm)

            })
            .catch(err => {
                console.log('err load class ', err);
            })

    }, [initForm]);

    return [formData]

}