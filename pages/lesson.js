
import React, { useEffect, useState, useRef } from 'react';
import { Menu, Row, Col, Button, Input, Spin, Radio, Modal, Form, Select, Table, Space, Card, Tabs, Breadcrumb, Switch } from 'antd';
import { get } from 'lodash';
import { DownOutlined, UpOutlined, PlusCircleOutlined, EditOutlined, DeleteOutlined } from '@ant-design/icons';
import Router, { useRouter, withRouter } from 'next/router';

import Layout from '../components/share/layout';
import request from '../handler/request';
import '../public/styles.css';
import Test from '../components/test';
import Document from "../components/DocumentLesson";
import LessonPart from "../components/Lesson";
import Lecture from "../components/Lecture"

const { TabPane } = Tabs;

const App = (props) => {
    const router = useRouter();
    const [activeKey, setActive] = useState('2');
    const [dataLesson, setDataLesson] = useState({})

    const [delayShow, setDelay] = useState(false);

    const [breadcrumb, setBreadcrum] = useState([]);

    useEffect(() => {
        requestLesson()
    }, [props.router.query]);

    const requestLesson = () => {
        const query = get(props, 'router.query', '');
        if (query && query.id) {
            request.get(`/admin/menus/${query.id}/lesson`)
                .then(({ data, book = {}, grade = {}, subject = {} }) => {
                    console.log(data, 'data====', book)
                    setDataLesson(data);
                    setBreadcrum([
                        {
                            title: get(grade, 'title', ''),
                            action: () => {
                                router.push({
                                    pathname: '/subject',
                                    query: {
                                        grade_id: get(grade, 'id', '')
                                    }
                                })
                            }
                        },
                        {
                            title: get(subject, 'title', ''),
                            action: () => {
                                router.push({
                                    pathname: '/book',
                                    query: {
                                        grade_id: get(grade, 'id', ''),
                                        subject_id: get(subject, 'id', '')
                                    }
                                })
                            }
                        },
                        {
                            title: get(book, 'title', ''),
                            action: () => {
                                router.push({
                                    pathname: '/menu',
                                    query: {
                                        book_id: get(book, 'id', ''),
                                        grade_id: get(grade, 'id', ''),
                                        subject_id: get(subject, 'id', '')
                                    }
                                })
                            }
                        }
                    ])
                })
                .catch(err => {
                    console.log('err load data lesson', err)
                })

        }
    }

    useEffect(() => {
        setTimeout(() => {
            setDelay(true);
        }, 100)
    }, []);

    return (
        <Layout openKeys={router.pathname}>
            <div style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-between',
            }}>
                <Breadcrumb
                    separator=" > "
                    style={{ marginTop: 10, cursor: 'pointer' }}
                >
                    {breadcrumb.map(i => {
                        return <Breadcrumb.Item key={i.title} onClick={i.action}>{i.title}</Breadcrumb.Item>
                    })}
                </Breadcrumb>

                <Button
                    onClick={() => {
                        router.push({
                            pathname: '/table-content',
                            query: {
                                id: get(props, 'router.query.menu_id', ''),
                                path: get(props, 'router.query.path', ''),
                            }
                        })
                    }}
                    type="primary" style={{ marginTop: 5, marginRight: 30 }}> Back to menu </Button>

            </div>
            <Tabs onChange={setActive} activeKey={activeKey}>
                <TabPane tab="Sắp xếp bài học" key="0">
                    <LessonPart
                        setActive={setActive}
                        data={dataLesson}
                        activeKey={activeKey}
                    />
                </TabPane>
                <TabPane tab="Tài liệu" key="1">
                    <Document data={dataLesson} />
                </TabPane>
                <TabPane tab="Video" key="2">
                    <Lecture data={dataLesson} />
                </TabPane>
                <TabPane tab="Đề kiểm tra" key="3">
                    <Test data={dataLesson} />
                </TabPane>
            </Tabs>
            {/* modal */}
        </Layout>
    );
}


export default withRouter(App);
